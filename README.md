# WIMR-Conversions README <!-- omit in toc -->

## Contents

- [Contents](#contents)
- [NOTICE - THIS IS OUT-OF-DATE](#notice---this-is-out-of-date)
- [Introduction](#introduction)
- [Purpose](#purpose)
- [Cube Types](#cube-types)
  - [Polar](#polar)
  - [Cartesian](#cartesian)
- [Usage](#usage)
  - [Polar Grid Cube Creation](#polar-grid-cube-creation)
  - [Cartesian Grid Cube Creation](#cartesian-grid-cube-creation)
- [Implementation Details](#implementation-details)
  - [Retrieving Individual Sweep Data](#retrieving-individual-sweep-data)
  - [Writing MATLAB MAT files in Python](#writing-matlab-mat-files-in-python)
  - [Interpolation](#interpolation)
    - [Methods](#methods)
    - [Prerequisites](#prerequisites)
      - [Azimuth](#azimuth)
      - [Range](#range)
    - [Polar vs Cartesian](#polar-vs-cartesian)
  - [Performance Improvements](#performance-improvements)

## NOTICE - THIS IS OUT-OF-DATE

There have been some significant refactors to this software and this README needs to be updated. Issue #4.

## Introduction

This package contains Python classes that are useful especially for converting [Wave Imanging Marine Radar
NetCDF
files](https://gitlab.com/osu-nrsg/wimr-format-spec/-/blob/master/Wave_Imaging_Marine_Radar_Format_spec.ad)
into MATLAB "cubes" typically used by the Nearshore Remote Sensing Group at Oregon State University.

## Purpose

Marine radar data typically contains individual radar pulse returns or "rays" recorded by a data acquisition
system as the radar operates in time. Marine radar typically is mounted on a spinning motor and has a fairly
narrow horizontal beam width. Thus each ray may be assigned a particular azimuth angle that together with the
radial distance of each sample in the ray, the radar's heading azimuth, and the radar's location allows the
backscatter intensity information to be geo-located.

For various radar imagery processing algorithms, it is useful to have the backscatter intensity of successive
sweeps interpolated to a regular static geographic grid of some form. This simplifies temporal analysis of
the backscatter intensity, for example averaging intensity through time.

The "cube" files created by these classes contain radar data that has been interpolated to a regular grid,
such that the cube.data field is a 3-D array with dimensions of (range, azimuth, sweep) (in MATLAB; dimension
order is reversed in C or Python).

## Cube Types

This package can create two different cube types: "polar" and "Cartesian".

### Polar

A polar cube simply contains the interpolation of the source data which is already in (range, azimuth)
coordinates to a constant (range, azimuth) mesh grid. If the radar is stationary (constant heading and
position) then polar interpolation provides static geographic positions for each range/azimuth point in the
cube grid and can maintain the natural resolution of the radar data (i.e. decreasing tangential resolution
with range).

### Cartesian

A Cartesian cube contains the interpolation of the source data to a Cartesian (x, y) mesh grid. The installed
altitude of the radar above the water is taken into effect when transforming the grid coordinates. This data
format has the advantage of being more easy to peform certain 3-D spatial-temporal analyses, but forces the
radar data which has a very fine tangential resolution near the radar and a coarse resolution far from the
radar to have a constant lateral (x, y) resolution.

## Usage

### Polar Grid Cube Creation

```python
from wimr_conversions import PolarCubeConfig, create_cube
# Get a CubeConfig object of the type you want
# The below config will create a grid (relative to the radar's heading)
config = PolarCubeConfig(azi_start=10, azi_end=180, azi_res=0.5)
# Create the cube
cube_path = create_cube("NewportTower_20201009_123456.nc", config)
```

### Cartesian Grid Cube Creation

```python
from wimr_conversions import CartCubeConfig, create_cube
# Get a CubeConfig object of the type you want
# The below config will create a grid (relative to the radar's heading and origin)
config = CartCubeConfig(x_bounds=[-2000, 2000], y_bounds=[0, 3000], d_xy=5, grid_name="mordor")
# Create the cube, saving with a different name in a different folder
cube_path = create_cube("MiddleEarth_20201009_123456.nc", config)
# cube file name will be MiddleEarth_20201009_123456_cart_mordor.mat
```

Both `PolarCubeConfig` and `CartCubeConfig` also have the following optional arguments:

- `skip_existing` - If `True` (default), if a cube of the same name already exists at the destination, it
  will not be recreated.
- `cube_dir` - If specified, the cube will be saved in this dir instead of the same dir as the WIMR file.
- `norm_limits` - If specifed, image normalization (dynamic range expansion) will be applied to the intensity
  before saving as uint8. Default is `(0, 255)`. For example, if set to `(160, 245)`, the intensities are
  offset and scaled such that intensities 160 or lesser become 0 and intensities 245 or greater become 255.

`create_cube` also has the following optional arguments:

- `cube_path` - Manually specify the path for the cube file rather than creating based on the WIMR path.
- `postproc_json` - Postprocessing paramters as a JSON string to be saved in the cube under
  `results/postProcConfig`.

## Implementation Details

### Retrieving Individual Sweep Data

The `return_intensity` variable in WIMR-format NetCDF files has two dimensions: `time` and `range`. The
`time` and `azimuth` variables only have the `time` dimension. The `sweep_start_ray_index` and
`sweep_end_ray_index` variables have a `sweep` dimension and provide the start and stop indices for a given
sweep in the `time` dimension. That is, if `sweep_start_ray_index[4]` is `1476` and `sweep_end_ray_index[4]`
is `1854`, then data for sweep `4` from the variables with the `time` dimension is retrieved from dataset
`ds` as follows:

```python
sweep_intensity = ds["return_intensity"][1476:1855, :]
sweep_azi = ds["azimuth"][1476:1855]
sweep_time = ds["time"][1476:1855]
```

The `ncvarslice.NCVarSlicer` class converts the sweep ray index variables into Python `slice` objects to
conveniently subdivide these variables into sweeps:

```python
from ncvarslice import NCVarSlicer
slicer = NCVarSlicer(ds["sweep_start_ray_index"], ds["sweep_end_ray_index"])

# Get data for sweep 5
sweep_intensity = ds["return_intensity"][slicer[4], :]
sweep_azi = ds["azimuth"][slicer[4]]
sweep_time = ds["time"][slicer[4]]

# print the first time in each sweep
for sweep_i, slc in enumerate(slicer[:]):
    print(f"Sweep {sweep_i}: {ds['time'][slc][0]}")
    # or
    print(f"Sweep {sweep_i}: {ds['time'][slc.start]}")
```

### Writing MATLAB MAT files in Python

MATLAB's MAT file v7.3 format for storing data uses HDF5 as the underlying data storage format (as does
NetCDF and thus the WIMR format, incidentally), with some added metadata to describe the MATLAB variables.
Thankfully Freja Nordsiek's [hdf5storage](https://github.com/frejanordsiek/hdf5storage) package takes care of
the specific implentation details of making a MATLAB-compatible HDF5 MAT file. However, there are some
caveats.

1. The `hdf5storage.savemat` function writes variables to the MAT file all-at-once; that is, it does not
   allow for incrementally appending data to a given variable. This is problematic as we may not be able to
   read in and process all the data from the WIMR file at once.
2. The `hdf5storage.savemat` function tries preserve the apparent dimension order for variables. Since MATLAB
   uses [column-major order](https://en.wikipedia.org/wiki/Row-_and_column-major_order) and Python uses
   row-major order, this means that if a NumPy array with dimensions (x, y, z) is saved to a MAT file with
   `savemat`, the dimensions are transposed on disk so that in MATLAB the array appears to have the same
   dimension order (x, y, z). We would prefer to use the dimension ordering native to each language to
   maximize efficient data access.

These issues are addressed as follows:

1. The CubeCreator writes the MAT file in two steps.
   1. A dictionary is created with all metadata as well as the output variables for the MAT file in their
      expected shapes as arrays of zeros. (NumPy is able to use considerably less memory for arrays of all
      zeros than arrays of varied values.) This dictionary is used to create the cube with the `savemat`
      function.
   2. The cube is opened in "append" (`r+`) mode as an HDF5 file using `h5py.File()` from the h5py library.
      As the source data is read from the WIMR file and interpolated the results are saved incrementally to
      the MAT file by setting their values in the `h5py.File` object.
2. After creating the dictionary in the first step of the MAT file creation, all multi-dimensional numpy
   arrays in the dictionary are transposed before calling `savemat`.

### Interpolation

#### Methods

The source radar data is mapped to a static geographic grid using interpolation. The `scipy.interpolation`
module provides various interpolators that may be used to interpolate data in various geometries. The
interpolators we are most interested in are:

- [`interp2d`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.interp2d.html#scipy.interpolate.interp2d)
  - Works with scattered source points or source points on a regular grid.
  - `call()` method of interpolant only accepts vectors for the `x` and `y` arguments, i.e. for a rectangular
    output grid. It will not work if interpolating to scattered output points.
  - May use linear, cubic, or quintic interpolation methods.
- [`RegularGridInterpolator`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.RegularGridInterpolator.html)
  - Source data must be on a regular grid
  - Allows for specification of a fill value for output points outside of the bounds of the source data grid.
  - Output point locations provided to `call()` may be scattered or on a grid.
  - May use linear or nearest-neighbor interpolation methods.
- [`RectBivariateSpline`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.RectBivariateSpline.html)
  - Source data must be on a regular grid.
  - Output point locations provided to `call()` may be scattered or if `grid=True` is set, may be grid
    vectors.
  - Uses spline interpolation.

#### Prerequisites

There are two requirements that must be met before interpolating a sweep of radar data onto a set of output
query points:

1. If using gridded interpolation methods, i.e. where the source data is on a rectilinear or curvilinear
   grid, then each grid vector must be monotonically increasing.
   - We generally only use gridded methods if possible, as scattered interpolation involves
     computationally-expensive Delaunay triangulation.
2. The source data and query points must be in the same coordinate system.

These must be discussed for both the range and azimuth coordinates.

##### Azimuth

In order to meet requirement 1, there must be an azimuth chosen as the artificial "zero" azimuth, with all
successive azimuths in the source grid increasing from there. Fortunately, we have this value in the WIMR
file as the variable `start_azi`, which is the configured or default (0) azimuth boundary between sweeps in
the recording. It is important to know that `start_azi` is relative to the radar's heading, whereas the
values in the `azimuth` variable are relative to True North (unless the platform is moving, which is a
separate case we'll deal with later). Using the terms _azi<sub>TrueN</sub>_ for azimuth relative to True
North, _azi<sub>raw</sub>_ for azimuth relative to heading, and _azi<sub>0</sub>_ for azimuth relative to
`start_azi`, we can transform between them with the following equations:

_azi<sub>raw</sub>_ = _azi<sub>TrueN</sub>_ - _heading_

_azi<sub>0</sub>_ = ( _azi<sub>raw</sub>_ - _start_azi_ ) **mod 360°**

Since each sweep is bounded by `start_azi`, we know that applying **mod 360°** should make the azimuths all
monitonically increasing in the range **[0, 360)**.

Thus to meet reqs. 1 and 2 the source azimuth vector and the query azimuth points must both be converted to
_azi<sub>0</sub>_ before interpolation.

##### Range

On a small vessel, marine radar is typically installed less than ten meters above the water surface. However
a wave-imaging marine radar may be mounted 30 m or more above the water surface. The distance to each range
bin reported by a marine radar is calculated from the round-trip time for each transmitted radar pulse and
its reflection. This line-of-sight range may be referred to as the "slant range" to differentiate it from the
distance from the radar at sea level to each range bin on the ocean surface, aka "ground range". Ground range
may be determined from the following equations:

If &nbsp;_r<sub>slant</sub>_ ≤ _h<sub>ASL</sub>_ &nbsp;&nbsp;:&nbsp;&nbsp; _r<sub>ground</sub>_ = 0

If &nbsp;_r<sub>slant</sub>_ > _h<sub>ASL</sub>_ &nbsp;&nbsp;:&nbsp;&nbsp; _r<sub>ground</sub>_ = √ (
  _r<sub>slant</sub>_<sup>2</sup> - _h<sub>ASL</sub>_<sup>2</sup>)

Where _h<sub>ASL</sub>_ is the height of the radar above sea level.

The source range vector is already recorded as slant range, so to meet requirements 1 and 2, query range
points must be converted to slant range.

#### Polar vs Cartesian

For interpolating from source data in polar coordinates to query points in a polar grid is mostly
straightforward. The query range and azimuth must be transformed as per the previous section. However, rather
than specifying a query range explicitly, the source data range vector is used as the range vector for the
polar cubes as it is already invariant. The slant range is recorded in the cube variable `range_pol` and the
ground range is recorded in the cube variable `Rg`. Then any of the previously-mentioned interpolation
functions may be used to interpolate the source data to the query grid, depending on which is fastest
(typically `interp2d`).

Interpolating to a Cartesian grid requires a few more steps. It is possible to transform the source data
coordinates to Cartesian coordinates, however this would result in a scattered source dataset and therefore
require the slow Delaunay triangulation. Instead, it is better to transform the query grid points into the
(_azi<sub>0</sub>_, _r<sub>slant</sub>_) coordinate system, in which case the simpler bilinear interpolation
methods may be used. This requires creating meshgrid variables from the x and y domains of the source data,
converting these to standard polar coordinates (_ϕ_, _r_) where _r_ is _r<sub>ground</sub>_, and then
transforming these to (_azi<sub>0</sub>_, _r<sub>slant</sub>_).

This results in scattered query points, as opposed to the gridded query points for polar cubes. Thus
`RegularGridInterpolator` or `RectBivariateSpline` must be used, depending on which is fastest (typically
`RegularGridInterpolator`).

_TODO: Double check which interpolation methods are best/fastest._

### Performance Improvements

It is not incorrect to read, interpolate, and save the results of one sweep at a time, however there are some
ways to improve performance.

1. **Read more than one sweep from the WIMR file at once.** [This
   article](https://scoutapm.com/blog/understanding-disk-i-o-when-should-you-be-worried) explains why it is
   costly to perform repeated read-operations from disk--that is, it can take a long time (especially with
   mechanical hard disks) to locate the region of interest on the disk and read the data. Depending on the
   application there may be different solutions to this problem, but in the case of making cubes the
   preferred tactic is to read a large chunk of the source data into memory, and then when that data has been
   processed and saved to the cube file, read the next chunk. The challenge is determining the best chunk
   size, which depends on the free memory on the system, as well as the additional memory needed to save
   intermediate objects before the results are written to disk.
2. **Use concurrency and multiple cores to perform interpolation calculations in parallel.** The task of
   interpolating one radar sweep does not depend on previous or future sweeps. Furthermore, this
   interpolation is a CPU-intensive task, so it may be sped-up by using multiple CPU cores in parallel.

_TODO: Write more about parallelization._
