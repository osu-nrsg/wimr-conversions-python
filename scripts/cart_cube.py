import json
import logging

import utm
from ncdataset import NCDataset

from wimr_conversions import CartCubeParams, create_cube

logging.basicConfig(level=logging.DEBUG)

gbu_x = [412534, 414434]  # UTM Easting bounds (same zone as origin)
gbu_y = [4937389, 4943119]  # UTM Northing bounds (same zone as origin)
grid_name = "fftRegion"  # Mandatory if creating multiple cart cubes from one recording
d_xy = 10  # grid spacing in meters. 10 if not specified

wimr_path = "/data/nwpt_compare/hpxd/NewportTower_20201119_191205.nc"
norm_limits = [160.0, 255.0]
cube_dir = "/data/nwpt_compare/cubes/hpxd_160-255"

# wimr_path = "/data/nwpt_compare/isr/NewportTower_20203241800.nc"
# norm_limits = [0, 255.0]
# cube_dir = "/data/nwpt_compare/cubes/isr"

with NCDataset(wimr_path) as ds:
    origin_lat = ds.get_scalar_var("latitude")
    origin_lon = ds.get_scalar_var("longitude")

origin_E, origin_N, *_ = utm.from_latlon(origin_lat, origin_lon)
x_bounds = tuple(x - origin_E for x in gbu_x)
y_bounds = tuple(y - origin_N for y in gbu_y)

spectrum_config = {
    "center_E": 413632,  # UTM Easting (m) of the center of the roi
    "center_N": 4942403,  # UTM Northing (m) of the center of the roi
    "name": "Nye Beach",  # name for the roi
    "size": 128,  # length of each side of the roi in pixels
    "h": 18.6,  # estimated average depth in the roi in meters
    "h_datum": "",  # Datum for the depth measurement, if available.
}

cfg = CartCubeParams(
    cube_dir=cube_dir,
    skip_existing=False,
    norm_limits=norm_limits,
    x_bounds=x_bounds,
    y_bounds=y_bounds,
    d_xy=d_xy,
    grid_name=grid_name,
)

create_cube(wimr_path, cfg, postproc_json=json.dumps({"spectrum": spectrum_config}))
