"""Test interpolation methods serially to see which are fastest"""

# %%
import time

import numpy as np
from ncdataset import NCDataset
from ncvarslice import VarSlice
from scipy.interpolate import RectBivariateSpline, RegularGridInterpolator, interp2d

from wimr_conversions.util import cart2pol

wimr_ds = NCDataset("/data/nwpt_compare/isr/NewportTower_20203241800.nc")
slicer = VarSlice(wimr_ds["sweep_start_ray_index"], wimr_ds["sweep_end_ray_index"])

# %% Load data

intensity_s = []
time_s = []
azi0_s = []
nsum = wimr_ds["summing_factor"][:]
uint8_divisor = wimr_ds["summing_factor"][:] * (2 ** wimr_ds["daq_bit_depth"][0] / 2**8)


r_slant = wimr_ds["range"][:]
start_azi = wimr_ds.get_scalar_var("start_azi", default=0)
for slc in slicer[:]:
    intensity_s.append(wimr_ds["return_intensity"][slc, :])
    time_s.append(wimr_ds["time"][slc])
    azi0_s.append(
        np.mod(wimr_ds["azimuth"][slc] - wimr_ds.site_heading - start_azi, 360.0)
    )

print("Loaded.")

# %% Setup polar interp domain
azi0_q_pol = np.arange(0, 190.5, 0.5)
r_slant_q = r_slant.copy()

# %% interp2d

t_a = time.time()

intensity_pol_i2 = []
for sweep_i in range(len(intensity_s)):
    f = interp2d(r_slant, azi0_s[sweep_i], intensity_s[sweep_i])
    result = np.uint8(
        f(r_slant_q, azi0_q_pol, assume_sorted=True) / uint8_divisor[sweep_i]
    )
    intensity_pol_i2.append(result)
t_b = time.time()
print(f"interp2d done: {t_b - t_a:.1f} seconds")


# %% Gridded RectBivariateSpline

t_a = time.time()
intensity_rbvi_pol = []
for sweep_i in range(len(intensity_s)):
    f = RectBivariateSpline(azi0_s[sweep_i], r_slant, intensity_s[sweep_i])
    result = np.uint8(f(azi0_q_pol, r_slant_q, grid=True) / uint8_divisor[sweep_i])
    intensity_rbvi_pol.append(result)
t_b = time.time()
print(f"Gridded RectBivariateSpline done: {t_b - t_a:.1f} seconds")


# %% Setup cart interp domain
xdom = np.arange(-3150.0, -1240, 10)
ydom = np.arange(-4364.0, 1376.0, 10)
x_q, y_q = np.meshgrid(xdom, ydom, indexing="ij")
phi_q, r_q = cart2pol(x_q, y_q)
azi_q = 90 - phi_q * 180 / np.pi
azi0_q = np.mod(azi_q - wimr_ds.site_heading - start_azi, 360.0)
r_slant_q = np.sqrt(r_q**2 + wimr_ds["altitude"][0] ** 2)

# %% Scattered RectBivariateSpline
t_a = time.time()
intensity_rbvs = []
for sweep_i in range(len(intensity_s)):
    f = RectBivariateSpline(azi0_s[sweep_i], r_slant, intensity_s[sweep_i])
    result = np.uint8(f(azi0_q, r_slant_q, grid=False) / uint8_divisor[sweep_i])
    intensity_rbvs.append(result)
t_b = time.time()
print(f"Scattered RectBivariateSpline done: {t_b - t_a:.1f} seconds")

# %% Scattered RegularGridInterpolator
t_a = time.time()
intensity_rgi = []
for sweep_i in range(len(intensity_s)):
    f = RegularGridInterpolator(
        (azi0_s[sweep_i], r_slant), intensity_s[sweep_i], bounds_error=False
    )
    result = np.uint8(f((azi0_q, r_slant_q)) / uint8_divisor[sweep_i])
    intensity_rgi.append(result)
t_b = time.time()
print(f"Scattered RegularGridInterpolator done: {t_b - t_a:.1f} seconds")

# %%
