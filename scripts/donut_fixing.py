from pathlib import Path

from ncdataset import NCDataset

import wimr_conversions

wimr_paths = Path("/data/nwpt_compare/hpx").glob("*.nc")
dst_dir = Path("/data/nwpt_compare/hpxd")

expected_trigger_offset = -369.0

for wimr_path in wimr_paths:
    print("Correcting", wimr_path)
    with NCDataset(wimr_path) as wimr_ds:
        current_offset = wimr_ds["daq_trigger_correction"][0]
    adjustment_m = expected_trigger_offset - current_offset
    adjustment_samples = int(adjustment_m / 3)
    donut = (-adjustment_samples) + 1
    dst_path = dst_dir / wimr_path.name
    wimr_conversions.add_donut(wimr_path, donut, dst_path)
    print(f"{wimr_path} done.")
