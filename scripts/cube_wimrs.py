import logging
from pathlib import Path

import wimr_conversions
from wimr_conversions import PolarCubeParams

logging.basicConfig(level=logging.DEBUG)

wimr_paths = Path("/data/new").glob("NewportTower*.nc")
cfg = PolarCubeParams(
    skip_existing=False,
    # azi_end=189.0,
    # norm_limits=[160, 255],
    # cube_dir="/data/nwpt_compare/cubes/hpxd_160-255",
)

for wimr_path in wimr_paths:
    print(f"Cubing {wimr_path}...")
    wimr_conversions.create_cube(wimr_path, cfg)
    print("...done.")
print("All done.")
