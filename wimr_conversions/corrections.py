import os
import shutil
import tempfile
from contextlib import ExitStack, contextmanager
from pathlib import Path

from ncdataset import NCDataset
from netCDF4 import Variable as NCVar  # type: ignore

from wimr_conversions._types import PathLike


@contextmanager
def temp_file(suffix=None, prefix=None, dir_=None, text=False):
    """Context manager wrapper of tempfile.mkstemp"""
    fd, fpath = tempfile.mkstemp(suffix, prefix, dir_, text)
    try:
        yield fd, Path(fpath)
    finally:
        os.close(fd)


def add_donut(wimr_path: PathLike, donut: int, new_path: PathLike | None = None):
    """Remove some number of extra samples from the beginning of a wimr recording where
    the trigger offset was not set correctly

    Parameters
    ----------
    wimr_path
        Path to WIMR NetCDF dataset
    donut
        1-based index of the first range bin to *keep*. So remove the first sample from
        every ray, provide a donut of 2. (This odd numbering is used for legacy reasons.)
    new_path
        Path to save the WIMR dataset with the donut. Overwrites the initial dataset if
        not provided.
    """
    wimr_path = Path(wimr_path)
    new_path = Path(new_path) if new_path else wimr_path
    if donut < 1:
        return
    tmp_path: Path
    with ExitStack() as estack:
        # Open temp path, wimr dataset and new dataset
        if new_path == wimr_path:
            _, tmp_path = estack.enter_context(
                temp_file(suffix=".nc", dir_=wimr_path.parent)
            )
        else:
            tmp_path = new_path
        wimr_ds: NCDataset = estack.enter_context(NCDataset(wimr_path, mode="r"))
        new_ds: NCDataset = estack.enter_context(NCDataset(tmp_path, "w"))
        # Check if site_donut has already been set. This fn doesn't handle that case.
        if wimr_ds.site_donut > 1:
            raise NotImplementedError("Haven't implemented changing the donut.")
        # Copy dimensions, variable definitions, global attributes, and finally variables
        new_nsamples = wimr_ds.dimensions["range"].size - donut + 1
        _add_donut_copy_dims(wimr_ds, new_ds, new_nsamples)
        _add_donut_create_vars(wimr_ds, new_ds, new_nsamples)
        _add_donut_copy_gatts(wimr_ds, new_ds, donut)
        _add_donut_copy_vars(wimr_ds, new_ds, donut)
        # If a temp file was created new_path is wimr_path so we're overwriting the
        # original.
        if tmp_path != new_path:
            shutil.move(str(tmp_path), str(new_path))


def _add_donut_copy_dims(old_ds: NCDataset, new_ds: NCDataset, new_nsamples: int):
    """Copy all dimensions from old_ds to new_ds except with the new range size."""
    for dimname, dim in old_ds.dimensions.items():
        if dimname == "range":
            size = new_nsamples
        elif dim.isunlimited():
            size = None
        else:
            size = dim.size
        new_ds.createDimension(dimname, size=size)


def _add_donut_create_vars(old_ds: NCDataset, new_ds: NCDataset, new_nsamples: int):
    """Create vars in new_ds based on old_ds except with the new range size."""
    var: NCVar
    for varname, var in old_ds.variables.items():
        # enable compression and checksumming for all variables.
        var_kwargs = var.filters()
        var_kwargs["zlib"] = True
        var_kwargs["complevel"] = 6
        var_kwargs["fletcher32"] = True
        # chunking should read/write efficiency if set correctly.
        var_kwargs["chunksizes"] = None
        if varname == "return_intensity":
            var_kwargs["chunksizes"] = [1, new_nsamples]
        var_kwargs["fill_value"] = getattr(var, "_FillValue", None)
        # Create the var
        newvar = new_ds.createVariable(
            varname, var.datatype, var.dimensions, **var_kwargs
        )
        # Copy the variable attributes.
        for attrname in var.ncattrs():
            if attrname != "_FillValue":
                newvar.setncattr(attrname, var.getncattr(attrname))


def _add_donut_copy_gatts(old_ds: NCDataset, new_ds: NCDataset, donut: int):
    """Copy all global attrs from old_ds to new_ds and set the new site_donut value."""
    for attrname in old_ds.ncattrs():
        if attrname == "site_donut":
            new_ds.setncattr(attrname, donut)
        else:
            new_ds.setncattr(attrname, old_ds.getncattr(attrname))


def _add_donut_copy_vars(old_ds: NCDataset, new_ds: NCDataset, donut: int):
    """Copy all variables from old_ds to new_ds but remove the donut"""
    new_range = old_ds["range"][: (-donut + 1)]
    data_r_slice = slice(donut - 1, None)
    for varname, var in old_ds.variables.items():
        if "range" not in var.dimensions:
            new_ds[varname][...] = var[:]
        elif varname == "range":
            new_ds[varname][...] = new_range
        elif varname == "return_intensity":
            new_ds[varname][...] = var[:, data_r_slice]
