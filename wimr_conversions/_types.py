from numbers import Integral as Integral
from numbers import Number as Number  # reexport
from pathlib import Path, PosixPath

import numpy as np
from numpy.typing import NDArray

# from typing_extensions import TypeIs

type PathLike = Path | str
type PosixPathLike = PosixPath | str

# numeric types
type Float = float | np.floating
type IntArray = NDArray[np.integer]
type FloatArray = NDArray[np.floating]
type NumArray = NDArray[np.number]


# def isfloat_array(val) -> TypeIs[FloatArray]:
#     return isinstance(val, np.ndarray) and issubclass(val.dtype.type, np.floating)


# def isint_array(val) -> TypeIs[IntArray]:
#     return isinstance(val, np.ndarray) and issubclass(val.dtype.type, np.integer)
