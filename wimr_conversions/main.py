from pathlib import Path
from typing import Type, overload

from wimr_conversions._types import PathLike

from .cube_params import (
    CartCubeParams,
    CubeParamsBase,
    MovingPolarCubeParams,
    PolarCubeParams,
)
from .cubing2 import BaseCube
from .errors import CubingError
from .moving import MovingPolarCube
from .stationary import CartCube, PolarCube

cube_params_map: dict[Type[CubeParamsBase], Type[BaseCube]] = {
    CartCubeParams: CartCube,
    MovingPolarCubeParams: MovingPolarCube,
    PolarCubeParams: PolarCube,
}


@overload
def create_cube(
    wimr_path: PathLike,
    params: PolarCubeParams,
    cube_path: Path | None = None,
    postproc_json: str = "",
    update_wimr_proc_info=True,
) -> PolarCube: ...


@overload
def create_cube(
    wimr_path: PathLike,
    params: CartCubeParams,
    cube_path: Path | None = None,
    postproc_json: str = "",
    update_wimr_proc_info=True,
) -> CartCube: ...


@overload
def create_cube(
    wimr_path: PathLike,
    params: MovingPolarCubeParams,
    cube_path: Path | None = None,
    postproc_json: str = "",
    update_wimr_proc_info=True,
) -> MovingPolarCube: ...


def create_cube(
    wimr_path: PathLike,
    params: CubeParamsBase,
    cube_path: Path | None = None,
    postproc_json: str = "",
    update_wimr_proc_info=True,
) -> BaseCube:
    """Create an interpolated MATLAB "cube" .mat file from a WIMR recording

    Parameters
    ----------
    wimr_path
        str or Path to the WIMR file
    config
        Structure of cubing configuration parameters
    cube_path
        If specified, the path to save the cube mat file. Default is to create a filename
        from the WIMR file.
    postproc_json
        Postprocessing configuration parameters to save in the cube's
        results/postProcConfig field (as a JSON string)
    update_wimr_proc_info
        If true, add `params` to the processing_info attribute in the wimr file

    Returns
    -------
    cube_info
        CubeInfo object with info about the completed cube.

    Raises
    ------
    CubingError
        Some problem occurred building the cube
    """
    wimr_path = Path(wimr_path).resolve(strict=True)
    try:
        Cube = cube_params_map[type(params)]
    except KeyError:
        raise TypeError(
            f"{type(params)} is not a valid type for the params argument"
        ) from None
    cube = Cube(wimr_path, params, cube_path=cube_path, postproc_json=postproc_json)
    if not cube.skip:
        try:
            cube.build()
        except Exception as ex:
            cube.cube_path.unlink(missing_ok=True)
            raise CubingError(
                f"Error building cube {cube.cube_path}. The MAT file has been deleted."
            ) from ex
        cube.update_wimr_proc_info()
    return cube
