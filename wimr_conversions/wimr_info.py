"""cube_info.py - Contains the CubeInfo class."""

from __future__ import annotations

from dataclasses import dataclass

import numpy as np
import pandas as pd
from ncdataset import NCDataset
from ncvarslice import NCVarSlicer

from .util import rslant2ground


@dataclass(init=False)
class WIMRInfo:
    """A WIMRInfo structure contains WIMR recording data needed to set up cubing"""

    #: Slicer to get sweep-by-sweep data
    slicer: NCVarSlicer

    # WIMR dimension info
    #: Number of sweeps in the recording
    n_sweep: int
    #: Number of summed rays recorded in the recording
    n_time: int
    #: Number of range bins in each ray
    n_range: int
    #: Number of summed rays in each sweep (-9999 if non-constant)
    rays_per_sweep: int
    #: Average number of rays in each sweep
    avg_rays_per_sweep: float
    #: Maximum number of rays in each sweep
    max_rays_per_sweep: int

    # Origin
    #: Recording origin latitude
    latitude: float
    #: Recording origin longitude
    longitude: float
    #: Recording origin altitude
    altitude: float

    #: recording range bins
    r_slant: np.ndarray
    #: Ground range bins from r_slant and altitude
    r_ground: np.ndarray
    #: Radar heading (from True N) for stationary radar
    heading: float
    #: Sweep boundary azimuth, relative to the radar heading
    start_azi_raw: float
    #: Offset of radar from platform heading, for moving radar
    platform_heading_offset: float
    #: Is the radar moving
    platform_is_mobile: bool

    #: recording ray times
    time: pd.Series
    #: recording sweep start times
    sweep_time: pd.Series

    #: recording ray azimuths
    azimuth: pd.Series

    #: Bits per sample
    bit_depth: int
    bytes_per_sample: int

    #: Shape of the raw time array
    rawtime_shape: tuple[int, int]

    #: Average number of samples per sweep
    avg_sweep_nsamples: float

    def __init__(self, wimr_ds: NCDataset):
        # Calculate remaining vars
        with wimr_ds:
            # Slicer for getting sweep data
            self.slicer = NCVarSlicer(
                wimr_ds["sweep_start_ray_index"], wimr_ds["sweep_end_ray_index"]
            )

            # Dimension info
            self.n_sweep = wimr_ds.dimensions["sweep"].size
            self.n_time = wimr_ds.dimensions["time"].size
            self.n_range = wimr_ds.dimensions["range"].size
            self.rays_per_sweep = wimr_ds.get_scalar_var(
                "rays_per_sweep"
            )  # will be -9999 if non-constant
            self.avg_rays_per_sweep = (
                self.rays_per_sweep
                if self.rays_per_sweep > 0
                else np.ceil(self.n_time / self.n_sweep)
            )
            self.max_rays_per_sweep = max(slc.stop - slc.start for slc in self.slicer)

            # Location
            self.latitude = wimr_ds.get_scalar_var("latitude")
            self.longitude = wimr_ds.get_scalar_var("longitude")
            self.altitude = wimr_ds.get_scalar_var("altitude")

            # Spatial info
            self.r_slant = wimr_ds["range"][:].astype(np.float64)
            self.heading = wimr_ds.site_heading
            self.start_azi_raw = wimr_ds.get_scalar_var("start_azi", default=0.0)
            self.platform_heading_offset = getattr(wimr_ds, "platform_heading_offset", 0)
            self.platform_is_mobile = (
                getattr(wimr_ds, "platform_is_mobile", "false") == "true"
            )

            # Time
            start_t = pd.to_datetime(wimr_ds.get_scalar_var("time_coverage_start"))
            self.time = pd.Series(
                start_t + pd.to_timedelta(wimr_ds["time"][:], unit="s"), name="Radar Time"
            )
            self.sweep_time = pd.Series(
                [self.time[slc].iloc[0] for slc in self.slicer], name="Sweep Time"
            )

            # Azimuth
            self.azimuth = pd.Series(wimr_ds["azimuth"], name="Azimuth", index=self.time)

            # Other
            self.bit_depth = wimr_ds.get_scalar_var("daq_bit_depth")
            self.bytes_per_sample = wimr_ds["return_intensity"].dtype.itemsize

        self.r_ground = rslant2ground(self.r_slant, self.altitude)
        # If rays_per_sweep is greater than zero, then we know that the raw data has a
        # constant number of collections per sweep, so we'll be able to save the original
        # time matrix. Otherwise we have to just sub in timeInt. Thus the cube still works
        # with scripts that use "time" instead of timeInt. Yes, this is enabling bad
        # behavior...
        if self.rays_per_sweep > 0:
            self.rawtime_shape = (self.n_sweep, self.rays_per_sweep)
        else:
            self.rawtime_shape = (self.n_sweep, self.max_rays_per_sweep)

        self.avg_sweep_nsamples = self.n_range * self.avg_rays_per_sweep
