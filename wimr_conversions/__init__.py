import logging
from pathlib import Path as _Path

from single_version import get_version as _get_version
from tblib import pickling_support

from .corrections import add_donut
from .cube_params import CartCubeParams, MovingPolarCubeParams, PolarCubeParams
from .cubing2 import MAX_CHUNK_NSWEEPS as _MAX_CHUNK_NSWEEPS
from .cubing2 import RUN_SERIAL as _RUN_SERIAL  # for debugging
from .main import create_cube
from .moving import MovingPolarCube
from .stationary import CartCube, PolarCube

pickling_support.install()  # make sure all exceptions in this module are pickleable

logger = logging.getLogger(__name__)

__version__ = _get_version(__name__, _Path(__file__).parent.parent)

# TODO: docstrings needed throughout
