from __future__ import annotations

import logging
from dataclasses import dataclass
from multiprocessing.managers import SharedMemoryManager

import numpy as np
from scipy.interpolate import RegularGridInterpolator, interp1d, interp2d
from shared_ndarray2 import SharedNDArray, shared_ndarray

from .util import img_norm

logger = logging.getLogger(__name__)


@dataclass(init=False)
class ChunkOfSweeps:
    """A segment of data from a WIMR file read in for processing all-at-once.

    For memory reasons it may not always possible to read-in an entire WIMR file.
    ChunkOfSweeps simply encapsulates the data from a chunk of the file that has been
    read-in. If `mem_mgr` is provided, the largest data items (intensity, time, and azi0)
    are saved in SharedArrays so other (multiprocessing) processes may access these large
    chunks using shared memory.

    Attributes
    ----------
    intensity
        WIMR return_intensity, with dimensions (rays, range_bins)
    time
        WIMR time, with dimensions (rays,)
    azi0
        WIMR azimuth relative to start_azi, with dimensions (rays,)
    r_slant
        WIMR range, with dimensions (range_bins,)
    uint8_divisor
        The value by which the intensity is divided to convert to 0..255. Dimensions
        (sweeps,)
    norm_limits
        Max and min values for applying image normalization to the intensity in the range
        of 0..255
    ray_slices
        List of slice objects to get the intensity, time, and azi0 for a given sweep.
        Dimensions (sweeps,)

    Initialization Variables
    ------------------------
    mem_mgr
        Optional `multiprocessing.managers.SharedMemoryManager`. If provided, `intensity`,
        `time`, and `azi0` are converted to `SharedNDArray` instances on initialiaztion.
    """

    __slots__ = {
        "intensity",
        "time",
        "azi0",
        "r_slant",
        "uint8_divisor",
        "norm_limits",
        "ray_slices",
    }
    intensity: np.ndarray | SharedNDArray
    time: np.ndarray | SharedNDArray
    azi0: np.ndarray | SharedNDArray
    r_slant: np.ndarray
    uint8_divisor: np.ndarray
    norm_limits: tuple[float, float]
    ray_slices: list[slice]

    def __init__(
        self,
        *,
        intensity: np.ndarray,
        time: np.ndarray,
        azi0: np.ndarray,
        r_slant: np.ndarray,
        uint8_divisor: np.ndarray,
        norm_limits: tuple[float, float],
        ray_slices: list[slice],
        mem_mgr: SharedMemoryManager | None = None,
    ):
        self.intensity = intensity
        self.time = time
        self.azi0 = azi0
        self.r_slant = r_slant
        self.uint8_divisor = uint8_divisor
        self.norm_limits = norm_limits
        self.ray_slices = ray_slices
        if mem_mgr:
            self.intensity = shared_ndarray.from_array(mem_mgr, self.intensity)
            self.time = shared_ndarray.from_array(mem_mgr, self.time)
            self.azi0 = shared_ndarray.from_array(mem_mgr, self.azi0)

    def get_sweep(
        self, sweep_i: int
    ) -> tuple[
        np.ndarray, np.ndarray, np.ndarray, np.ndarray, float, tuple[float, float]
    ]:
        """Get the data from a sweep in the chunk

        Parameters
        ----------
        sweep_i
            Index of the sweep (of the chunk) from which to get the data
        """
        return (
            self.intensity[self.ray_slices[sweep_i], :],
            self.time[self.ray_slices[sweep_i]],
            self.azi0[self.ray_slices[sweep_i]],
            self.r_slant,
            self.uint8_divisor[sweep_i],
            self.norm_limits,
        )


@dataclass
class SweepInterpolator:
    """Class to interpolate a WIMR sweep, possibly in multiple processes using shared
    memory to exchange data.

    Attributes
    ----------
    chunk_sweep_i
        The index of the sweep in the chunk to interpolate in this instance
    chunk
        The structure of sweeps of radar data, possibly using SharedArrays for some of the
        items to minimize the amount of data copied to new processes
    azi0_q
        The query azimuth points for the interpolation, either as an ndarray or a
        SharedArray
    r_slant_q
        The query range points for interpolation, either as an ndarray or a SharedArray
    intensity_out
        The destination array for interpolated intensity, for this chunk
    time_out
        Optional destination array for interpolated time (if azi0q and r_slant_q are
        vectors - gridded interpolation)
    azi_oob_out
        Optional destination array for out-of-bounds azimuth logical array (if azi0q and
        r_slant_q are vectors - gridded interpolation)
    """

    # data from the WIMR file
    chunk_sweep_i: int
    chunk: ChunkOfSweeps
    # query points
    azi0_q: np.ndarray | SharedNDArray
    r_slant_q: np.ndarray | SharedNDArray
    # output arrays for the MAT file
    intensity_out: np.ndarray | SharedNDArray
    # time_mat and azi_oob not provided for scattered (Cartesian) interpolation
    time_out: np.ndarray | SharedNDArray | None = None
    azi_oob_out: np.ndarray | SharedNDArray | None = None

    def interpolate(self) -> int:
        """Equivalent to the __call__ method"""
        return self()

    def __call__(self) -> int:
        """Interpolate a sweep in the chunk, normalize it and save it to the output
        arrays"""
        # Get the data for this sweep
        (
            intensity_in,
            time_in,
            azi0_in,
            r_slant_in,
            uint8_divisor,
            norm_limits,
        ) = self.chunk.get_sweep(self.chunk_sweep_i)

        if self.azi0_q.ndim > 2:
            azi0_q = self.azi0_q[self.chunk_sweep_i, ...]
            r_slant_q = self.r_slant_q[self.chunk_sweep_i, ...]
        else:
            azi0_q = self.azi0_q[:]
            r_slant_q = self.r_slant_q[:]

        # Interpolate the sweep
        intensity_out, time_out, azi_oob, _ = interp_sweep(
            intensity_in,
            time_in,
            azi0_in,
            r_slant_in,
            azi0_q,
            r_slant_q,
        )
        intensity_out /= uint8_divisor  # convert to 0..255
        img_norm(
            intensity_out, norm_limits[0], norm_limits[1], maxval=255, nanval=0.0
        )  # perform image normalization
        # Save the data to the output arrays
        self.intensity_out[self.chunk_sweep_i, ...] = intensity_out.round().astype(
            "uint8"
        )
        if self.time_out and self.azi_oob_out:
            if time_out is not None:
                self.time_out[self.chunk_sweep_i, ...] = time_out
            if azi_oob is not None:
                self.azi_oob_out[self.chunk_sweep_i, ...] = azi_oob
        return self.chunk_sweep_i


def interp_sweep(
    intensity_in: np.ndarray,
    time_in: np.ndarray | None,
    azi_in: np.ndarray,
    rg_in: np.ndarray,
    azi_q: np.ndarray,
    rg_q: np.ndarray,
) -> tuple[np.ndarray, np.ndarray | None, np.ndarray | None, np.ndarray | None]:
    """Perform gridded or scattered interpolation of sweep intensity and time data.

    Parameters
    ----------
    intensity_in
        Sweep intensity data, with dimensions (n_rays, n_samples)
    time_in
        Sweep ray time vector, with dimensions (n_rays,)
    azi_in
        Sweep ray azimuth vector, with dimensions (n_rays,)
    rg_in
        Ray range vector, with dimensions (nsamples,)
    azi_q
        Azimuth query points, either vector for gridded interpolation or matrix for
        scattered
    rg_q
        Range query points, either vector for gridded interpolation or matrix for
        scattered
    Returns
    -------
    intensity_out
        Interpolated intensity as a uint8 matrix
    time_out
        If not scattered, the interpolated time vector
    azi_oob
        If not scattered, the query azimuths outside the bounds of the sweep's azimuths
    aziq_rq_oob
        logical array (mask) same shape as intensity with True for out-of bounds azi_q or
        r_q

    Raises
    ------
    ValueError
        If azi0_in or rg_in are not monotonically increasing
    """
    # create the azimuth mask to exclude any bad (non-increasing) azimuths
    # TODO: Maybe try to unwrap first
    if np.any(np.diff(azi_in) <= 0):  # non-increasing azimuths?
        azi_mask = np.ones_like(azi_in, dtype=bool)
        azi_mask[1:] = np.diff(azi_in) > 0
        azi_in = azi_in[azi_mask]
        intensity_in = intensity_in[azi_mask, :]
        if time_in is not None:
            time_in = time_in[azi_mask]
        logger.debug("Removed %d non-increasing azimuths", np.sum(~azi_mask))

    # Make sure azi_in and rg_in are monotonically increasing
    if not (np.all(np.diff(azi_in) > 0) and np.all(np.diff(rg_in) > 0)):
        raise ValueError("azi_in and rg_in must be monotonically increasing.")

    # Interpolate the intensity
    scattered = azi_q.shape == rg_q.shape and len(azi_q.shape) > 1
    if scattered:
        f = RegularGridInterpolator(
            (azi_in, rg_in), intensity_in, bounds_error=False, fill_value=np.nan
        )
        intensity_out = f((azi_q, rg_q))
    else:
        f = interp2d(
            rg_in, azi_in, intensity_in, copy=False, bounds_error=False, fill_value=np.nan
        )
        intensity_out = f(rg_q, azi_q, assume_sorted=True)

    # Find query azi and r that are outside the data bounds
    aziq_oob = (azi_q > np.max(azi_in)) | (azi_q < np.min(azi_in))
    rq_oob = (rg_q > np.max(rg_in)) | (rg_q < np.min(rg_in))
    # Set out-of bounds query locations to zero
    aziq_rq_oob = np.zeros_like(intensity_out, dtype=bool)
    if not scattered:
        aziq_rq_oob[aziq_oob, :] = True
        aziq_rq_oob[:, rq_oob] = True
    else:
        aziq_rq_oob = aziq_oob | rq_oob

    # Interpolate time, if requested
    time_out = None
    if not scattered:
        f_t = interp1d(
            azi_in,
            time_in,
            copy=False,
            fill_value="extrapolate",  # type: ignore
            assume_sorted=True,
        )
        time_out = f_t(azi_q)

    # set return vars
    azi_oob = None if scattered else aziq_oob
    return intensity_out, time_out, azi_oob, aziq_rq_oob
