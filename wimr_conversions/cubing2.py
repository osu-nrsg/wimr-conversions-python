"""cubers.py -- Classes for cubing WIMR files"""

import json
import logging
import multiprocessing.pool
from abc import ABC, abstractmethod
from collections import defaultdict
from contextlib import ExitStack
from datetime import datetime, timezone
from functools import lru_cache
from multiprocessing.managers import SharedMemoryManager
from pathlib import Path
from typing import Any

import h5py
import hdf5storage
import numpy as np
import psutil
import utm
from ncdataset import NCDataset
from pydantic import validate_call
from shared_ndarray2 import SharedNDArray, shared_ndarray

from .cube_params import CartCubeParams, CubeParamsBase, PolarCubeParams
from .sweep import ChunkOfSweeps, SweepInterpolator
from .util import (
    datetime2matlabdn,
    dict_nparrays_transpose,
    dict_scalars_to_double,
    offset_slice,
    rslant2ground,
    uint_divisor,
)
from .wimr_info import WIMRInfo

logger = logging.getLogger(__name__)

# Debug values
MAX_CHUNK_NSWEEPS: int | None = None
RUN_SERIAL = False


def wc_version():
    """Return the version of the wimr_conversions package"""
    # Import here to prevent circular import
    from . import __version__

    return __version__


def now_utc() -> datetime:
    return datetime.now(tz=timezone.utc)


class BaseCube(ABC):
    """OSU-NRSG radar cube creator"""

    #: Path to the NetCDF WIMR recording
    wimr_path: Path
    #: Parameters for processing
    params: CubeParamsBase
    #: Intended postprocessing parameters for the cube to save for reprocessing
    postproc_json: str

    #: The recording in WIMR NetCDF format
    wimr_ds: NCDataset
    #: Path for the new cube
    cube_path: Path

    #: Cube exists and should not be remade
    skip: bool
    #: Recording info needed to set up cubing
    info: WIMRInfo

    #: Cube origin latitude
    origin_lat: float
    #: Cube origin longitude
    origin_lon: float

    # Cube query azimuth coordinates, relative to the origin (possibly moving)
    #: azi_q is relative to the site heading (zero for moving platform, so true N)
    azi_q: np.ndarray
    #: Range query points, in slant range
    r_slant_q: np.ndarray
    #: Range query points, in ground range
    r_ground_q: np.ndarray

    #: Dict structure to initialize MAT file with hdf5storage.savemat
    mat_dict: dict[str, Any]

    @validate_call
    def __init__(
        self,
        wimr_path: Path,
        params: CubeParamsBase,
        cube_path: Path | None = None,
        postproc_json: str = "",
    ):
        """Initialize the cuber.

        Extend in subclass with subclass-specific params type.
        Actual subclass init should go in post_init().

        Parameters
        ----------
        wimr_path
            Path to radar recording - WIMR NetCDF dataset
        params
            Parameters for cubing
        """
        self.wimr_path = wimr_path
        self.params = params
        self.wimr_ds = NCDataset(wimr_path, mode="r", keepopen=False)
        self.postproc_json = postproc_json

        self.info = WIMRInfo(self.wimr_ds)

        self.cube_path = cube_path or self.get_cube_path()
        self.skip = self.params.skip_existing and self.cube_path.is_file()
        if self.skip:
            logger.info(f"Skipping existing cube {self.cube_path}.")
            return
        self.mat_dict = defaultdict(dict)  # easily make subdicts
        self.post_init()

    def post_init(self):
        """Subclass init steps go here"""
        pass

    def get_cube_path(self):
        cube_name = self.gen_cube_name()
        if self.params.cube_dir:
            if self.params.date_dir:
                # Get the date the recording started from the wimr file
                with self.wimr_ds:
                    wimr_date = self.wimr_ds["time_coverage_start"][0][:10]
                cube_dir = self.params.cube_dir / wimr_date
            else:
                cube_dir = self.params.cube_dir
            try:
                cube_dir.mkdir(exist_ok=True, parents=True)
            except PermissionError as ex:
                raise PermissionError(
                    ex.errno, "Permission denied to create dir", ex.filename
                ) from None
        else:
            cube_dir = self.wimr_ds.nc_path.parent
        return cube_dir / cube_name

    def build(self):
        """Build the cube. Subclasses should extend this method to do the actual cube
        building."""
        ispolar = isinstance(self.params, PolarCubeParams)
        self.origin_lat, self.origin_lon = self.get_cube_origin()
        self.init_output_domains()
        self.init_matdict()
        self.init_matfile()
        with ExitStack() as exit_stack:
            # open context managers for the WIMR recording, the MAT file, a memory
            # manager, and a multiproc pool (equivalent to with NCDataset(stuff...) as
            # wimr_ds, h5py.File(stuff...) as mat_ds, etc.)
            wimr_ds: NCDataset = exit_stack.enter_context(self.wimr_ds)
            mat_ds: h5py.File = exit_stack.enter_context(
                h5py.File(self.cube_path, mode="r+")
            )
            mem_mgr: SharedMemoryManager = exit_stack.enter_context(SharedMemoryManager())  # type: ignore
            # pool after mem_mgr so pool is closed first, then shared memory released.
            pool: multiprocessing.pool.Pool = exit_stack.enter_context(
                multiprocessing.pool.Pool()
            )

            # aliases, with type narrowing
            mat_data = mat_ds["data"]
            mat_timex = mat_ds["timex"]
            mat_time = mat_ds["time"]
            assert isinstance(mat_data, h5py.Dataset)
            assert isinstance(mat_timex, h5py.Dataset)
            assert isinstance(mat_time, h5py.Dataset)
            mat_Azi_oob = mat_ds["Azi_oob"] if ispolar else None
            mat_timeInt = mat_ds["timeInt"] if ispolar else None

            # Figure out chunk size
            chunk_max_nsweep = self.calc_chunk_nsweeps()
            # limit to the max debug value, if set.
            if MAX_CHUNK_NSWEEPS and chunk_max_nsweep > MAX_CHUNK_NSWEEPS:
                chunk_max_nsweep = MAX_CHUNK_NSWEEPS
            sweep_shape = (
                self.azi_q.shape
                if self.azi_q.ndim > 1
                else (len(self.azi_q), len(self.r_slant_q))
            )
            #: Array to sum output sweeps to make timex
            timex_sum = np.zeros(sweep_shape, dtype="uint64")

            logger.debug("Starting processing.")
            for chunk_start_sweep_i in range(0, self.info.n_sweep, chunk_max_nsweep):
                logger.debug("Getting chunk data")
                # Determine the size of the chunk and get the chunk data
                remaining_nsweep = self.info.n_sweep - chunk_start_sweep_i
                chunk_nsweep = (
                    remaining_nsweep
                    if remaining_nsweep <= chunk_max_nsweep
                    else chunk_max_nsweep
                )
                chunk_sweep_slice = slice(
                    chunk_start_sweep_i, chunk_start_sweep_i + chunk_nsweep
                )
                chunk = self.get_chunk(chunk_sweep_slice, mem_mgr)

                intensity_out, time_out, azi_oob_out = self.get_output_shared_arrays(
                    mem_mgr, chunk_nsweep, sweep_shape
                )
                azi0_q_sa, r_slant_q_sa = self.get_chunk_output_domains(
                    mem_mgr, chunk_sweep_slice
                )

                # Create and run the interpolators
                interpolators = (
                    SweepInterpolator(
                        i,
                        chunk,
                        azi0_q_sa,
                        r_slant_q_sa,
                        intensity_out,
                        time_out,
                        azi_oob_out,
                    )
                    for i in range(chunk_nsweep)
                )
                logger.debug("Interpolating chunk sweeps")
                if not RUN_SERIAL:
                    for result in pool.imap_unordered(
                        SweepInterpolator.interpolate, interpolators
                    ):
                        logger.debug("Finished sweep %d", chunk_start_sweep_i + result)
                else:
                    for interpolator in interpolators:
                        result = interpolator()
                        logger.debug("Finished sweep %d", chunk_start_sweep_i + result)
                # Save the chunk results to the mat file
                mat_data[chunk_sweep_slice, ...] = intensity_out[:]
                timex_sum += intensity_out[:].sum(axis=0)
                if ispolar:
                    assert (
                        isinstance(mat_timeInt, h5py.Dataset)
                        and isinstance(mat_Azi_oob, h5py.Dataset)
                        and time_out
                        and azi_oob_out
                      )  # assure typechecker
                    timeInt_posix = self.info.time[0].timestamp() + time_out[:]  # noqa : N803
                    mat_timeInt[chunk_sweep_slice, :] = timeInt_posix
                    mat_Azi_oob[chunk_sweep_slice, :] = azi_oob_out[:]
            # End chunk loop
            # Also save the raw time
            for sweep_i in range(self.info.n_sweep):
                sweep_time = wimr_ds["time"][self.info.slicer[sweep_i]]
                time_slice = slice(len(sweep_time))
                mat_time[sweep_i, time_slice] = self.info.time[0].timestamp() + sweep_time
            mat_timex[:] = timex_sum / self.info.n_sweep
        # close exit stack

    def get_cube_origin(self) -> tuple[float, float]:
        """Get the origin latitude and longitude for the cube

        Override in moving cubes.
        """
        return self.info.latitude, self.info.longitude

    @abstractmethod
    def init_output_domains(self):
        """Generate the output domains for the type of cubing and save to the class

        For Cartesian cubing types, generates  x_dom, y_dom, relative to the origin and
        North (from get_lla)

        For all cubing types, generates azi0_q, r_slant_q, relative to the origin and the
        radar heading (or N for moving)
        """
        ...

    def init_matfile(self):
        logger.debug("Initializing MAT file.")
        # Transpose any numpy arrays in mat_dict since savemat assumes we want to keep the
        # apparent dimension order in MATLAB (we don't).
        dict_nparrays_transpose(self.mat_dict)
        # Make sure all scalars are doubles (as is typical in MATLAB)
        dict_scalars_to_double(self.mat_dict)
        # Create the MAT file
        hdf5storage.savemat(str(self.cube_path), self.mat_dict)
        logger.debug("MAT file init done.")

    def calc_chunk_nsweeps(self, *, mem_frac: float = 0.25, pow2: bool = True) -> int:
        """Determine the maximum number of sweeps that we allow to be read in at once

        Parameters
        ----------
        mem_frac
            The maximum fraction of available memory to allocate
        pow2
            if True, the returned value must be a power of two.

        Returns
        -------
        int
            Maxium number of sweeps to read in at once
        """
        mem_avail = psutil.virtual_memory().available
        sweep_bytes = self.info.avg_sweep_nsamples * self.info.bytes_per_sample
        if self.info.platform_is_mobile:
            # Moving platform needs new azi and range output domains for each sweep and
            # this is big.
            sweep_bytes += 2 * (self.info.avg_sweep_nsamples * 8)
        max_sweeps = int(mem_avail * mem_frac / sweep_bytes)
        if pow2:
            max_sweeps = int(2 ** np.floor(np.log2(max_sweeps)))  # power of 2
        return max_sweeps

    def get_chunk(self, sweep_slc: slice, mem_mgr: SharedMemoryManager) -> ChunkOfSweeps:
        """Get a chunk of radar data to work on.

        Parameters
        ----------
        sweep_slc
            Slice of the full sweep index to get as a chunk
        mem_mgr
            SharedMemoryManager to use if using shared memory
        """
        # Get a slice for all the rays in the chunk
        sweep_start_i: int = sweep_slc.start
        sweep_stop_i: int = sweep_slc.stop - 1
        chunk_slice_start_ray_i = self.info.slicer[sweep_start_i].start
        chunk_slice_stop_ray_i = self.info.slicer[sweep_stop_i].stop
        ray_slc = slice(chunk_slice_start_ray_i, chunk_slice_stop_ray_i)
        # Get slices to use on the chunk (0 is the beginning of the chunk)
        chunk_ray_slices = [
            offset_slice(slc, -chunk_slice_start_ray_i)
            for slc in self.info.slicer[sweep_slc]
        ]
        # Create the chunk
        chunk_sumfactors: np.ndarray = self.wimr_ds["summing_factor"][sweep_slc]
        return ChunkOfSweeps(
            intensity=self.wimr_ds["return_intensity"][ray_slc, :],
            time=self.wimr_ds["time"][ray_slc],
            azi0=self.get_chunk_azi(ray_slc),
            r_slant=self.info.r_slant,
            uint8_divisor=uint_divisor(chunk_sumfactors, self.info.bit_depth),
            norm_limits=self.params.norm_limits,
            ray_slices=chunk_ray_slices,
            mem_mgr=mem_mgr,
        )

    def get_output_shared_arrays(self, mem_mgr, chunk_nsweep, sweep_shape):
        intensity_shape = (chunk_nsweep, *sweep_shape)
        intensity_out = shared_ndarray.from_shape(mem_mgr, intensity_shape, np.uint8)
        if isinstance(self.params, PolarCubeParams):
            time_shape = intensity_shape[:2]
            time_out = shared_ndarray.from_shape(mem_mgr, time_shape, np.float64)
            azi_oob_out = shared_ndarray.from_shape(mem_mgr, time_shape, np.bool_)
        else:
            # timeInt and Azi_oob not set in Cartesian nor moving cubing types
            time_out = None
            azi_oob_out = None
        return intensity_out, time_out, azi_oob_out

    def get_chunk_output_domains(
        self, mem_mgr: SharedMemoryManager, chunk_sweep_slice: slice
    ) -> tuple[SharedNDArray[np.float64], SharedNDArray[np.float64]]:
        """Get the output domains for a given chunk, as SharedNDArrays.

        This may be overridden in subclasses.

        Parameters
        ----------
        mem_mgr
            Active SharedMemoryManager instance
        chunk_sweep_slice
            Slice of sweeps in this chunk

        Returns
        -------
        azi0_q_sa, r_slant_q_sa
            SharedNDArrays for the query azi0 and r_slant points
        """
        return self._get_constant_output_domains(mem_mgr)

    @lru_cache
    def _get_constant_output_domains(self, mem_mgr):
        azi0_q_sa = shared_ndarray.from_array(mem_mgr, self.azi_q)
        r_slant_q_sa = shared_ndarray.from_array(mem_mgr, self.r_slant_q)
        return azi0_q_sa, r_slant_q_sa

    def get_chunk_azi(self, ray_slc: slice) -> np.ndarray:
        """Get the azimuths for the chunk. This may be extended in subclasses.

        Parameters
        ----------
        sweep_slc
            The slice of the overall sweep index to get sweeps
        """
        return np.mod(
            self.wimr_ds["azimuth"][ray_slc]
            - self.info.heading
            - self.info.start_azi_raw,
            360,
        )

    @abstractmethod
    def gen_cube_name(self) -> str:
        """Generate the name of the cube from the wimr file path and the config"""
        ...

    @abstractmethod
    def init_matdict(self):
        """Initialize the MAT file for filling with data.

        Subclasses should extend this parent method then do subclass-specific things.
        """
        logger.debug("Initializing MAT file data.")
        mat_dict = self.mat_dict
        with self.wimr_ds as wimr_ds:
            mat_dict["results"]["heading"] = self.info.heading
            mat_dict["results"]["startAzi"] = self.info.start_azi_raw
            mat_dict["results"]["endAzi"] = wimr_ds.get_scalar_var(
                "end_azi", default=-9999.0
            )
            mat_dict["results"]["platform_heading_offset"] = (
                self.info.platform_heading_offset
            )
            mat_dict["results"]["platform_is_mobile"] = self.info.platform_is_mobile

            # Cube origin
            northing, easting, zone_no, zone_let = utm.from_latlon(
                self.origin_lat, self.origin_lon
            )
            mat_dict["results"]["XOrigin"] = northing
            mat_dict["results"]["YOrigin"] = easting
            mat_dict["results"]["UTMZone"] = f"{zone_no}{zone_let}"
            mat_dict["results"]["ZOrigin"] = self.info.altitude

            # Various info
            mat_dict["results"]["doughnut"] = float(wimr_ds.site_donut)
            mat_dict["results"]["RunLength"] = wimr_ds.computed_run_time
            mat_dict["results"]["TrueRPM"] = wimr_ds.computed_TrueRPM
            mat_dict["results"]["fullAfile"] = wimr_ds.computed_full_A_file == "true"
            mat_dict["results"]["cubingConfig"] = self.params.model_dump_json()
            mat_dict["results"]["postProcConfig"] = self.postproc_json
            mat_dict["results"]["softwareVersions"] = (
                getattr(wimr_ds, "software_versions", "")
                + f"\nwimr-conversions: {wc_version()}"
            )

            # Radar info
            mat_dict["radar"]["pulseWidth"] = wimr_ds["pulse_width"][0]
            mat_dict["radar"]["PRF"] = 1 / wimr_ds["prt"][0]
            mat_dict["radar"]["instrumentName"] = getattr(wimr_ds, "instrument_name", "")
            mat_dict["radar"]["verticalBeamWidth"] = wimr_ds[
                "antenna_vertical_beam_width"
            ][0]
            mat_dict["radar"]["horizontalBeamWidth"] = wimr_ds[
                "antenna_horizontal_beam_width"
            ][0]
            mat_dict["radar"]["frequency"] = wimr_ds["frequency"][0]

            # Get start time
            start_t = self.info.time.iloc[0]
            matdn = datetime2matlabdn(start_t)
            mat_dict["results"]["start_time"] = {
                "year": float(start_t.year),
                "month": float(start_t.month),
                "day": float(start_t.day),
                "hour": float(start_t.hour),
                "minute": float(start_t.minute),
                "second": float(start_t.second) + start_t.microsecond / 1e6,
                "epoch": start_t.timestamp(),
                "dateNum": matdn,
                "Zone": "UTC",
            }

            # Fill the header struct
            mat_dict["header"]["collections"] = float(self.info.rays_per_sweep)
            mat_dict["header"]["samples"] = float(
                self.info.n_range + wimr_ds.site_donut - 1
            )
            mat_dict["header"]["waveforms"] = float(wimr_ds["summing_factor"][0])
            mat_dict["header"]["collectionsMod"] = float(
                mat_dict["header"]["collections"]
            )
            mat_dict["header"]["rotations"] = float(self.info.n_sweep)
            mat_dict["header"]["file"] = wimr_ds.M_file
            mat_dict["header"]["gateDelay"] = float(wimr_ds["daq_trigger_delay"][0])
            mat_dict["header"]["radarType"] = wimr_ds.instrument_name
            mat_dict["header"]["wimrFile"] = wimr_ds.nc_path.name

            # Fill the daqConfig struct
            mat_dict["daqConfig"]["hardwareType"] = wimr_ds.daq_hardware_type
            mat_dict["daqConfig"]["sampleRateMHz"] = (
                wimr_ds["daq_sampling_frequency"][0] / 1e6
            )
            mat_dict["daqConfig"]["summedPulses"] = float(wimr_ds["summing_factor"][0])
            mat_dict["daqConfig"]["gateDelay"] = float(wimr_ds["daq_trigger_delay"][0])
            mat_dict["daqConfig"]["triggerCorrectionMeters"] = float(
                wimr_ds.get_scalar_var("daq_trigger_correction", default=-9999)
            )
            mat_dict["daqConfig"]["collections"] = float(self.info.rays_per_sweep)
            mat_dict["daqConfig"]["samples"] = float(wimr_ds.dimensions["range"].size)
            mat_dict["daqConfig"]["Ch1"] = {"enabled": "yes"}
            mat_dict["daqConfig"]["Ch1"]["offset_mV"] = wimr_ds["daq_dc_offset"][0] * 1e3
            gain = wimr_ds["daq_gain"][0]
            if gain > 0:
                mat_dict["daqConfig"]["Ch1"]["amplifier"] = f"{gain:.2f} db"
            else:
                mat_dict["daqConfig"]["Ch1"]["amplifier"] = "no"
            if gain < 0:
                mat_dict["daqConfig"]["Ch1"]["attenuation"] = f"{gain:.2f} db"
            else:
                mat_dict["daqConfig"]["Ch1"]["attenuation"] = "no attenuation"
            mat_dict["daqConfig"]["Ch1"]["video_inverted"] = wimr_ds.get_scalar_var(
                "daq_video_inverted", default="normal"
            )
            mat_dict["daqConfig"]["Ch2"] = {
                "enabled": "no",
                "offset_mV": 0,
                "amplifier": "no",
                "attenuation": "no attenuation",
            }

            # Other stuff
            mat_dict["location"] = wimr_ds.site_name.replace(" ", "")
            mat_dict["longName"] = wimr_ds.site_longname
            # Get station name from the file name. FIXME: This is too fragile!
            mat_dict["station"] = wimr_ds.nc_path.name.split("_")[0]
            mat_dict["whencreated"] = now_utc().strftime("%d-%b-%Y %H:%M:%S UTC")
            swdr = getattr(wimr_ds, "site_wave_dir_range", [0, 360])
            if isinstance(swdr, np.ndarray):  # some old wimr files have something else
                mat_dict["site_wave_dir_range"] = swdr

    def update_wimr_proc_info(self):
        """Update the processing_info attribute of the WIMR file with the processing
        parameters used in this cube

        Raises
        ------
        json.JSONDecodeError
            The content of the processing_info attribute of the WIMR file is not valid
            JSON.
        """
        self.wimr_ds.close()
        with self.wimr_ds(mode="a"):
            proc_info_tag = (
                f"wimr-conversions_{self.__class__.__name__}"
                f"_params_{now_utc():%Y-%m-%dT%H:%M:%S}Z"
            )
            # Load the JSON content of processing_info. Replace with "{}" if missing OR an
            # empty string.
            proc_info = json.loads(getattr(self.wimr_ds, "processing_info", "{}") or "{}")
            # dump the params into a dict that can be serialized to json
            proc_info[proc_info_tag] = self.params.model_dump(mode="json")
            self.wimr_ds.processing_info = json.dumps(proc_info)


class BasePolarCube(BaseCube):
    def init_matdict(self):
        super().init_matdict()

        mat_dict = self.mat_dict
        n_azi0_q = len(self.azi_q)
        n_r_slant_q = len(self.r_slant_q)

        mat_dict["interp_opts"] = "pol"
        # Cube range vectors
        mat_dict["range_pol"] = self.r_slant_q
        mat_dict["Rg"] = self.r_ground_q
        # cube.Azi will always be azi_raw. azi_raw = azi0 + start_azi; azi_true = azi_raw
        # + heading;
        mat_dict["Azi"] = np.mod(self.azi_q + self.info.start_azi_raw, 360)
        mat_dict["Azi_oob"] = np.full([self.info.n_sweep, n_azi0_q], False, dtype="bool")
        data_shape = [self.info.n_sweep, n_azi0_q, n_r_slant_q]
        mat_dict["data"] = np.zeros(data_shape, "uint8")
        mat_dict["timex"] = np.zeros(data_shape[1:], "float64")
        timeInt_shape = [self.info.n_sweep, n_azi0_q]  # noqa  # N806
        mat_dict["timeInt"] = np.zeros(timeInt_shape, "float64")
        mat_dict["time"] = np.full(self.info.rawtime_shape, np.nan)


class BaseCartesianCube(BaseCube):
    x_dom: np.ndarray
    y_dom: np.ndarray

    def init_matdict(self):
        super().init_matdict()

        mat_dict = self.mat_dict
        mat_dict["interp_opts"] = "cart"

        # convention: "_q" variables are the "query" points, i.e. the coordinates the cube
        # will have
        assert isinstance(self.params, CartCubeParams)
        mat_dict["grid_name"] = self.params.grid_name
        mat_dict["d_xy"] = self.params.d_xy
        mat_dict["grid_x"] = mat_dict["results"]["XOrigin"] + self.x_dom
        mat_dict["grid_y"] = mat_dict["results"]["YOrigin"] + self.y_dom
        n_x = len(self.x_dom)
        n_y = len(self.y_dom)
        data_shape = [self.info.n_sweep, n_x, n_y]
        mat_dict["data"] = np.zeros(data_shape, "uint8")
        mat_dict["timex"] = np.zeros(data_shape[1:])
        mat_dict["time"] = np.full(self.info.rawtime_shape, np.nan)
