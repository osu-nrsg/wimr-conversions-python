"""cube_params.py

Contains CubeParams classes for creating structures that contain information about a
particular cubing configuration. Data from particular cubes is not stored in a CubeParams.
"""

from abc import ABC as AbstractBaseClass
from pathlib import Path
from typing import TYPE_CHECKING, Literal

from pydantic import AfterValidator, BaseModel, DirectoryPath, confloat
from typing_extensions import Annotated

if TYPE_CHECKING:  # sourcery skip: assign-if-exp
    FloatDegrees = float
else:
    FloatDegrees = confloat(ge=0, lt=360)


def check_norm_limits(v: tuple[float, float]):
    if v[0] >= v[1]:
        raise ValueError("The order of the scaling limits must be (lesser, greater)")
    if not all(0 <= v <= 255 for v in v):
        raise ValueError("The scaling limits must be in the range [0.0, 255.0]")
    return v


U8NormalizationLimits = Annotated[tuple[float, float], AfterValidator(check_norm_limits)]


class CubeParamsBase(AbstractBaseClass, BaseModel):
    """Abstract base class for *CubeParams classes to specify common attributes and
    required methods"""

    cube_dir: Path | None = None
    """Directory in which the cube should be saved"""
    date_dir: bool = True
    """If true, the cube is saved in a subdir YYYY-mm-dd of cube_dir"""
    skip_existing: bool = False
    """If true and the cube file already exists, it will not be recreated"""
    norm_limits: U8NormalizationLimits = (0.0, 255.0)
    """Upper and lower bounds of source data for scaling data to 0-255"""


class PolarCubeParams(CubeParamsBase):
    """Configuration parameters for polar cubing

    Attributes
    ----------
    cube_dir
        Folder in which to save the cube file (may not exist). Default is the same
        directory as the WIMR file. Inherited from CubingConfig.
    date_dir
        If cube_dir is specified, should the cube be saved in a dated folder in cube_dir?
        Default is True.
    skip_existing
        If true, the cube will not be created if a cube with the same filename exists in
        the cube_dir. Inherited from CubingConfig.
    norm_limits
        Limits for performing image normalization on float values between 0..255 before
        converting to uint8. (Like setting color limits). Default is (0, 255)
    azi_start
        Beginning of the interpolation azimuth domain, relative to the radar heading, in
        degrees.
    azi_end
        End of interpolation azimuth domain, relative to the radar heading, in degrees.
    azi_res
        Resolution of the interpolation azimuth domain, in degrees

    """

    azi_start: FloatDegrees = 0
    azi_end: FloatDegrees = 359.5
    azi_res: FloatDegrees = 0.5


class CartCubeParams(CubeParamsBase):
    """Configuration parameters for Cartesian cubing

    Attributes
    ----------
    cube_dir
        Folder in which to save the cube file (may not exist). Default is the same
        directory as the WIMR file. Inherited from CubingConfig.
    date_dir
        If cube_dir is specified, should the cube be saved in a dated folder in cube_dir?
        Default is True.
    skip_existing
        If true, the cube will not be created if a cube with the same filename exists in
        the cube_dir. Inherited from CubingConfig.
    norm_limits
        Limits for performing image normalization on float values between 0..255 before
        converting to uint8. (Like setting color limits). Default is (0, 255)
    x_bounds
        min/max boundaries for the interpolation domain, in meters East of the origin.
        Defaults to (-1000, 1000)
    y_bounds
        min/max boundaries for the interpolation domain, in meters North of the origin.
        Defaults to (-1000, 1000)
    d_xy
        step size for the x/y interpolation domain, in meters. Defaults to 10.0.
    grid_name
        Name of the interpolation domain, for the case where multiple cubes are created
        from the same recording. Default is "".
    """

    x_bounds: tuple[float, float] = (-1000.0, 1000.0)
    y_bounds: tuple[float, float] = (-1000.0, 1000.0)
    d_xy: confloat(gt=0) = 10.0  # type: ignore  # confloat doesn't typecheck...
    grid_name: str = ""


class MovingPolarCubeParams(CubeParamsBase):
    gps_dir: DirectoryPath
    gps_dirtype: Literal["abx-two", "vs330"]
    d_azi: float
