from pathlib import Path

import numpy as np

from .cube_params import CartCubeParams, PolarCubeParams
from .cubing2 import BaseCartesianCube, BasePolarCube
from .util import cart2pol, phi2azi, rground2slant, rslant2ground


class PolarCube(BasePolarCube):
    params: PolarCubeParams

    def __init__(
        self,
        wimr_path: Path,
        params: PolarCubeParams,
        cube_path: Path | None = None,
        postproc_json: str = "",
    ):
        super().__init__(wimr_path, params, cube_path, postproc_json)

    def gen_cube_name(self) -> str:
        """Generate the name of the cube from the wimr file path and the config"""
        return f"{self.wimr_path.stem}_pol.mat"

    def init_output_domains(self):
        """Generate the output domains for the type of cubing and save to the class

        Initializes azi_q relative to the origin and the radar heading, r_slant_q, and
        r_ground_q.
        """
        # aliases for readability
        azi_raw_end = self.params.azi_end
        azi_raw_start = self.params.azi_start
        d_azi = self.params.azi_res
        # for 360 degrees, go from 0..360 - d_azi
        if azi_raw_end == azi_raw_start:
            azi_raw_end -= d_azi
        # convert to azi0 (relative to recording start_azi)
        azi0_start = np.mod(azi_raw_start - self.info.start_azi_raw, 360.0)
        azi0_end = np.mod(azi_raw_end - self.info.start_azi_raw, 360.0)
        if azi0_end < azi0_start:
            raise ValueError(
                "The provided start and end azimuth parameters are outside the bounds of"
                " the recording."
            )
        # get output domains
        self.azi_q = np.arange(azi0_start, azi0_end + d_azi, d_azi, dtype=float)
        self.r_slant_q = self.info.r_slant
        self.r_ground_q = rslant2ground(self.r_slant_q, self.info.altitude)


class CartCube(BaseCartesianCube):
    params: CartCubeParams

    def __init__(
        self,
        wimr_path: Path,
        params: CartCubeParams,
        cube_path: Path | None = None,
        postproc_json: str = "",
    ):
        super().__init__(wimr_path, params, cube_path, postproc_json)

    def gen_cube_name(self) -> str:
        """Generate the name of the cube from the wimr file path and the config"""
        if self.params.grid_name != "":
            return f"{self.wimr_path.stem}_cart_{self.params.grid_name}.mat"
        else:
            return f"{self.wimr_path.stem}_cart.mat"

    def init_output_domains(self):
        """Generate the output domains for the type of cubing and save to the class

        Initializes x_dom and y_dom, azi_q, r_slant_q, and r_ground_q
        """
        # First generate x_dom and y_dom
        self.x_dom = np.arange(
            self.params.x_bounds[0],
            self.params.x_bounds[1] + self.params.d_xy,
            self.params.d_xy,
            dtype=float,
        )
        self.y_dom = np.arange(
            self.params.y_bounds[0],
            self.params.y_bounds[1] + self.params.d_xy,
            self.params.d_xy,
            dtype=float,
        )
        # Use x_dom and y_dom to calculate azi0_q and r_slant_q
        x_q, y_q = np.meshgrid(self.x_dom, self.y_dom, indexing="ij")
        phi_q, self.r_ground_q = cart2pol(x_q, y_q)
        azi_q = phi2azi(phi_q)  # azi from True N
        azi_raw_q = azi_q - self.info.heading  # azi relative to heading
        self.azi_q = np.mod(azi_raw_q - self.info.start_azi_raw, 360.0)
        self.r_slant_q = rground2slant(self.r_ground_q, self.info.altitude)
