"""circles.py - Functions to help find a circle inscribed by multiple circles"""

from typing import overload

import numpy as np
from hdmedians.geomedian import geomedian

from wimr_conversions._types import Float, FloatArray

from .util import pol2cart


def _circle_points(r: float, npts: int, phi0: float = 0) -> tuple[FloatArray, FloatArray]:
    """Get a set of X, Y points on a circle specified by a radius and number of points"""
    dphi = 2 * np.pi / npts
    phi = phi0 + np.arange(0, 2 * np.pi, dphi)
    x, y = pol2cart(phi, r)
    return x, y


@overload
def _dist_eucl(x: Float, y: Float) -> Float: ...
@overload
def _dist_eucl(x: FloatArray, y: Float | FloatArray) -> FloatArray: ...
@overload
def _dist_eucl(x: Float | FloatArray, y: FloatArray) -> FloatArray: ...
def _dist_eucl(x: Float | FloatArray, y: Float | FloatArray) -> Float | FloatArray:
    """Euclidean distance"""
    return np.sqrt(x**2 + y**2)


def _get_median_circle_origin(
    circles_x0: FloatArray,
    circles_y0: FloatArray,
    radius: float,
) -> tuple[float, float]:
    """Get the (geometric) median origin of a set of congruent circles and ensure that the
    median origin is within all the circles.

    Parameters
    ----------
    circles_x0
        X-coordinates of all circle origins
    circles_y0
        Y-coordinates of all circle origins
    radius
        Radius of all circles.

    Returns
    -------
    x_med, y_med
        Geometric median of all the circle origins

    Raises
    ------
    ValueError
        The median origin is not within one or more of the circles

    """
    x_med: float
    y_med: float
    x_med, y_med = geomedian(np.column_stack((circles_x0, circles_y0)), axis=0)
    # Check that the new origin is inside all the circles (and thus they all overlap).
    origin_dists = _dist_eucl(circles_x0 - x_med, circles_y0 - y_med)
    if any(origin_dists > radius):
        raise ValueError("The median origin is not inside all of the circles")
    return x_med, y_med


def largest_circle_in_circles(
    circles_x0: FloatArray,
    circles_y0: FloatArray,
    circles_r: float,
    fudge_factor: float = 0.99,
) -> tuple[float, float, float]:
    """Get the (nearly) largest circle enclosed by circles with origin vectors circles_x0
    and circles_y0, constant radius circles_r, and a fudge_factor (to ensure the circle is
    entirely inside).

    Parameters
    ----------
    circles_x0
        vector of x coordinates of circle origins
    circles_y0
        vector of y coordinates of circle origins
    circles_r
        Circle radius
    fudge_factor
        Adjustment factor to ensure that the area covered by the resultant circle is
        entirely enclosed by all the other circles.

    Returns
    -------
    x0, y0, r
        Circle origin coordinates and radius

    """
    x0, y0 = _get_median_circle_origin(circles_x0, circles_y0, circles_r)
    # Get the distances from the new origin to every point on every circle.
    circle_x, circle_y = _circle_points(circles_r, 128)
    circles_x = [xi + circle_x for xi in circles_x0]
    circles_y = [yi + circle_y for yi in circles_y0]
    dists = _dist_eucl(
        np.concatenate(circles_x) - x0,
        np.concatenate(circles_y) - y0,
    )
    # Use the smallest distance shrunk by fudge_factor as the final radius.
    return x0, y0, dists.min() * fudge_factor
