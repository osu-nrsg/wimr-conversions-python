"""Utility functions used by this package."""

from datetime import datetime, timedelta
from typing import Literal, overload

import numpy as np

from wimr_conversions._types import (
    Float,
    FloatArray,
    IntArray,
    Number,
)


def dict_nparrays_transpose(dict_: dict) -> None:
    """Transpose all numpy arrays in a dict, including nested dicts"""
    for key, value in dict_.items():
        if isinstance(value, np.ndarray):
            dict_[key] = value.T
        elif isinstance(value, dict):
            dict_nparrays_transpose(dict_[key])


def dict_scalars_to_double(mat_dict: dict) -> None:
    """Convert all scalar variables in mat_dict to double-precision (for MATLAB
    scripts)"""
    for field in mat_dict:
        if isinstance(mat_dict[field], Number):
            mat_dict[field] = np.double(mat_dict[field])
        elif isinstance(mat_dict[field], dict):
            dict_scalars_to_double(mat_dict[field])


@overload
def azi2phi(azi: Float) -> Float: ...
@overload
def azi2phi(azi: FloatArray) -> FloatArray: ...
def azi2phi(azi: Float | FloatArray) -> Float | FloatArray:
    """Convert azimuth angle[s] (CW from N) to a polar angle[s] (CCW from E)"""
    return np.deg2rad(90 - azi)


@overload
def phi2azi(phi: Float) -> Float: ...
@overload
def phi2azi(phi: FloatArray) -> FloatArray: ...
def phi2azi(phi: Float | FloatArray) -> Float | FloatArray:
    """Convert a polar angle[s] (CCW from E) to an azimuth angle[s] (CW from N)"""
    return 90.0 - np.rad2deg(phi)


def rslant2ground(
    r_slant: FloatArray,
    altitude: float,
    fill_value: float = 0.0,
) -> FloatArray:
    """Convert slant range array to ground range array for a given altitude.

    Slant ranges less than altitude are set to `fill_value` (default 0)."""
    r_ground = np.full_like(r_slant, fill_value=fill_value)
    np.sqrt(r_slant**2 - altitude**2, r_ground, where=(r_slant >= altitude))
    return r_ground


@overload
def rground2slant(r_ground: FloatArray, altitude: Float) -> FloatArray: ...
@overload
def rground2slant(r_ground: Float, altitude: Float) -> Float: ...
def rground2slant(r_ground: Float | FloatArray, altitude: Float) -> Float | FloatArray:
    """Convert ground range to slant range for a given altitude"""
    return np.sqrt(r_ground**2 + altitude**2)


def dunwrap(deg: FloatArray, discont=180, axis=-1) -> FloatArray:
    """Same as np.unwrap, but in degrees"""
    return np.rad2deg(np.unwrap(np.deg2rad(deg), np.deg2rad(discont), axis))


def datetime2matlabdn(t_in: datetime) -> float:
    """Convert a python datetime (tz-aware or tz-naive) to a MATLAB datenum (same zone, if
    present in t_in)"""
    mdn = t_in + timedelta(days=366)
    td_from_midnight = t_in - datetime(*t_in.timetuple()[:3], tzinfo=t_in.tzinfo)
    frac_s = td_from_midnight.seconds / (24.0 * 60.0 * 60.0)
    frac_us = t_in.microsecond / (24.0 * 60.0 * 60.0 * 1.0e6)
    return mdn.toordinal() + frac_s + frac_us


@overload
def cart2pol(x: Float, y: Float) -> tuple[np.floating, np.floating]: ...
@overload
def cart2pol(x: FloatArray, y: Float | FloatArray) -> tuple[FloatArray, FloatArray]: ...
@overload
def cart2pol(x: Float | FloatArray, y: FloatArray) -> tuple[FloatArray, FloatArray]: ...
def cart2pol(
    x: Float | FloatArray,
    y: Float | FloatArray,
) -> tuple[FloatArray, FloatArray] | tuple[np.floating, np.floating]:
    """Convert x, y into phi, r"""
    return np.arctan2(y, x), np.sqrt(x**2 + y**2)


@overload
def pol2cart(phi: Float, r: Float) -> tuple[np.floating, np.floating]: ...
@overload
def pol2cart(phi: FloatArray, r: Float | FloatArray) -> tuple[FloatArray, FloatArray]: ...
@overload
def pol2cart(phi: Float | FloatArray, r: FloatArray) -> tuple[FloatArray, FloatArray]: ...
def pol2cart(
    phi: Float | FloatArray,
    r: Float | FloatArray,
) -> tuple[FloatArray, FloatArray] | tuple[np.floating, np.floating]:
    """Convert (phi, r) into (x, y)"""
    return r * np.cos(phi), r * np.sin(phi)


@overload
def img_norm(
    values,
    vmin,
    vmax,
    maxval=...,
    clip=...,
    copy: Literal[False] = ...,
    dtype=...,
    nanval=...,
) -> None: ...
@overload
def img_norm(
    values,
    vmin,
    vmax,
    maxval=...,
    clip=...,
    copy: Literal[True] = ...,
    dtype=...,
    nanval=...,
) -> FloatArray: ...
def img_norm(
    values: FloatArray,
    vmin: float,
    vmax: float,
    maxval: float | int = 1.0,
    clip: bool = True,
    copy: bool = False,
    dtype=None,  # dtype is a pain to type-annotate
    nanval=None,
) -> FloatArray | None:
    """Linearly normalize an array of values into the [0.0, maxval] interval.

    This is a somewhat simplified version of matplotlib.colors.Normalize.

    Parameters
    ----------
    values
        Array of values to normalize. This is modified in-place.
    vmin
        Minimum value of window.
    vmax
        Maximum value of window.
    maxval
        Maximum value of normalized data. Default is 1.0.
    clip
        If True (default) values outside the window are forced to 0.0 or maxval.
    copy
        If true, the normalization is performed on a copy of `values` and returned.
        Otherwise `values` is normalized in-place.
    dtype
        If copy is true, this dtype will be used for the output array. Default is to match
        the input type.
    nanval
        If provided, set all NaN values to nanval.

    Returns
    -------
    values
        normalized values, if copy is True. Otherwise None.
    """
    if copy:
        if dtype is None:
            dtype = values.dtype
        output = values.astype(dtype)
    else:
        output = values  # output *is* values

    # Operations:
    # - Optionally clip to vmin, vmax
    # - Scale such that vmin is 0 and vmax is maxval
    # - set nan vlaues
    if clip:
        output[:] = maxval * (values.clip(vmin, vmax) - vmin) / (vmax - vmin)
    else:
        output[:] = maxval * (values - vmin) / (vmax - vmin)
    if nanval is not None:
        output[np.isnan(output)] = nanval

    # Nothing to return if not copying
    return output if copy else None


def offset_slice(
    slc: slice, offset: int, undef_start_ok: bool = False, undef_stop_ok: bool = False
) -> slice:
    """Apply some offset to the start and stop limits of a slice, checking for some
    possible errors.

    Typically, it is undefined behavior to offset a slice with a start or stop of None.
    However:

    - If undef_start_ok is True, it is assumed that the start of the slice is defined as 0
      and can be added to (though not subtracted from).
    - If undef_stop_ok is True, it is assumed that the stop of the slice is defined as the
      last index of the sequence and may be subtracted from (though not added to).

    """
    # calculate and check start
    if slc.start is None:
        if not undef_start_ok:
            raise ValueError("slice start is None and undef_start_ok is False")
        if offset < 0:
            raise ValueError(
                "When the slice start is None it indicates the beginning of a sequence"
                " and cannot be decreased"
            )
        # unspecified start means start from 0
        slc = slice(0, slc.stop)
    new_start = slc.start + offset
    if slc.start >= 0 and new_start < 0 or slc.start < 0 and new_start >= 0:
        raise ValueError(
            "Slice start cannot be offset from negative to positive or vis-versa"
        )

    # calculate and check stop
    if slc.stop is None:
        if not undef_stop_ok:
            raise ValueError("slice stop is None and undef_stop_ok is False")
        if offset > 0:
            raise ValueError(
                "When the slice stop is None it indicates the end of a sequence and"
                " cannot be increased"
            )
        new_stop = offset
    else:
        new_stop = slc.stop + offset
        if slc.stop >= 0 and new_stop < 0 or slc.stop < 0 and new_stop >= 0:
            raise ValueError(
                "Slice stop cannot be offset from negative to positive or vis-versa"
            )

    return slice(new_start, new_stop, slc.step)


@overload
def uint_divisor(
    sum_factor: int,
    bit_depth_in: int,
    bit_depth_out: int = 8,
) -> np.floating: ...
@overload
def uint_divisor(
    sum_factor: IntArray,
    bit_depth_in: int,
    bit_depth_out: int = 8,
) -> FloatArray: ...
def uint_divisor(
    sum_factor: int | IntArray,
    bit_depth_in: int,
    bit_depth_out: int = 8,
) -> np.floating | FloatArray:
    """Get the factor or factors by which to divide summed radar pulses of some bit depth
    to make the values in the range 0-255.

    Parameters
    ----------
    sum_factor
        Pulse summing factor value or array of values (for each sweep)
    bit_depth_in
        Bit depth of the source data
    bit_depth_out
        Bit depth for the final data. Default is 8 (0-255).
    """
    bit_depth_max = 2**bit_depth_in - 1
    u8_max = 2**bit_depth_out - 1
    value_max = sum_factor * bit_depth_max
    return value_max / u8_max
