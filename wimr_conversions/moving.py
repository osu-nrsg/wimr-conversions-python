from multiprocessing.managers import SharedMemoryManager
from pathlib import Path

import numpy as np
import pandas as pd
import pymap3d
from scipy.interpolate import interp1d
from shared_ndarray2 import SharedNDArray, shared_ndarray

from .circles import largest_circle_in_circles, pol2cart
from .cube_params import MovingPolarCubeParams
from .cubing2 import BasePolarCube
from .gps_ingest import get_gps_csv_data
from .util import azi2phi, cart2pol, dunwrap, phi2azi, rground2slant, rslant2ground


class MovingPolarCube(BasePolarCube):
    #: Cubing parameters
    params: MovingPolarCubeParams
    #: Max range of cube domain
    r_gnd_max: float
    #: Azimuth of the first ray of each sweep
    sweep_heading: np.ndarray
    #: heading-corrected azimuths, relative to sweep_heading values
    azi_sweep: np.ndarray
    #: Latitude for each sweep
    sweep_lat: np.ndarray
    #: Longitude for each sweep
    sweep_lon: np.ndarray

    def __init__(
        self,
        wimr_path: Path,
        params: MovingPolarCubeParams,
        cube_path: Path | None = None,
        postproc_json: str = "",
    ):
        super().__init__(wimr_path, params, cube_path, postproc_json)

    def post_init(self):
        """Get the GPS position and attitude data and determine the cube origin."""
        gps_position, gps_attitude = get_gps_csv_data(
            self.params.gps_dir,
            self.info.time.iloc[0],
            self.info.time.iloc[-1],
            dirtype=self.params.gps_dirtype,
        )
        self.sweep_heading, self.azi_sweep = self.merge_heading_data(gps_attitude)
        self.sweep_lat, self.sweep_lon = self.get_sweep_positions(gps_position)
        # ### Figure out the cube origin
        (self._lat0, self._lon0), self.r_gnd_max = self.get_largest_common_circle()

    def merge_heading_data(
        self, gps_attitude: pd.DataFrame
    ) -> tuple[np.ndarray, np.ndarray]:
        """Use the heading data to get heading of each sweep and the adjusted azimuths
        relative to each sweep heading

        Parameters
        ----------
        gps_attitude
            Dataframe of attitude information from get_gps_csv_data()

        Returns
        -------
        sweep_heading
            Azimuth of the first ray of each sweep
        azi_sweep
            Azimuths of each ray in each sweep relative to the sweep_heading values
        """
        # Get the ship position and attitude for the times in the recording
        # unwrap headings
        gps_attitude["True Heading corrected UW"] = dunwrap(
            np.mod(gps_attitude["True Heading"] + self.info.platform_heading_offset, 360)
        )
        # Get dataframe of azimuths and headings (NaNs where timestamps don't match
        # exactly)
        azi_headings = pd.DataFrame(
            {
                "Azimuth": self.info.azimuth,
                "Heading": gps_attitude["True Heading corrected UW"],
            }
        )
        # Interpolate heading to azimuth times
        azi_headings = azi_headings.interpolate(inplace=False).reindex(self.info.time)
        # Correct all azimuths to True N
        azimuth_true = azi_headings["Azimuth"] + azi_headings["Heading"]
        # Save the starting True N azimuth as sweep_headings
        sweep_heading = np.array(
            [azimuth_true[self.info.slicer[i]][0] for i in range(self.info.n_sweep)]
        )
        # Get azimuths relative to each sweep heading, such that they are monotonically
        # increasing Note that they may go past 360 if the vessel turns clockwise fast
        # enough in a given sweep
        azi_sweep = np.zeros_like(azimuth_true)
        for i, slc in enumerate(self.info.slicer):
            azi_sweep[slc] = dunwrap(np.mod(azimuth_true[slc] - sweep_heading[i], 360))
        return sweep_heading, azi_sweep

    def get_sweep_positions(self, gps_position) -> tuple[np.ndarray, np.ndarray]:
        """Determine the latitude and longitude of each sweep in the recording.

        Parameters
        ----------
        gps_position
            DataFrame of position data from get_gps_csv_data()

        Returns
        -------
        sweep_lat
            Sweep latitudes
        sweep_lon
            Sweep longitudes
        """
        # datetime64s have to be converted to float64.
        ll_t = gps_position.index.values.astype("float64")
        sweep_t = self.info.sweep_time.values.astype("float64")
        f_interp_lat = interp1d(ll_t, gps_position["Latitude"])
        f_interp_lon = interp1d(ll_t, gps_position["Longitude"])
        return f_interp_lat(sweep_t), f_interp_lon(sweep_t)

    def get_chunk_output_domains(
        self, mem_mgr: SharedMemoryManager, chunk_sweep_slice: slice
    ) -> tuple[SharedNDArray, SharedNDArray]:
        chunk_sweep_x, chunk_sweep_y, _ = pymap3d.geodetic2enu(
            self.sweep_lat[chunk_sweep_slice],
            self.sweep_lon[chunk_sweep_slice],
            0,  # type: ignore
            self.origin_lat,  # type: ignore
            self.origin_lon,  # type: ignore
            0,  # type: ignore
        )
        chunk_sweep_headings = self.sweep_heading[chunk_sweep_slice]

        domains = np.stack(
            [
                translate_pol(self.azi_q, self.r_ground_q, sweep_x, sweep_y, 0, 0)
                for (sweep_x, sweep_y) in zip(chunk_sweep_x, chunk_sweep_y)
            ],
            axis=1,
        )
        azi0_q = np.array(
            [
                np.mod(azi - chunk_sweep_headings[sweep_i], 360)
                for (sweep_i, azi) in enumerate(domains[0])
            ]
        )
        r_slant_q = rground2slant(domains[1], self.info.altitude)

        azi0_q_sa = shared_ndarray.from_array(mem_mgr, azi0_q)
        r_slant_q_sa = shared_ndarray.from_array(mem_mgr, r_slant_q)
        return azi0_q_sa, r_slant_q_sa

    def get_chunk_azi(self, dataset_slc: slice) -> np.ndarray:
        # Return the heading-corrected azimuths
        return self.azi_sweep[dataset_slc]

    def get_cube_origin(self) -> tuple[float, float]:
        """Get the origin latitude and longitude for the cube"""
        return self._lat0, self._lon0

    def init_output_domains(self):
        """Generate the output domains

        For MovingPolarCube, intializes azi_q, r_slant_q, and r_ground_q
        """
        self.azi_q = np.arange(0, 360, self.params.d_azi)
        r_slant_max = rground2slant(self.r_gnd_max, self.info.altitude)
        self.r_slant_q = np.arange(0, r_slant_max, np.diff(self.info.r_slant[:2]))
        self.r_ground_q = rslant2ground(self.r_slant_q, self.info.altitude)

    def gen_cube_name(self) -> str:
        """Generate the name of the cube from the wimr file path and the config"""
        return f"{self.wimr_path.stem}_moving_pol.mat"

    def get_largest_common_circle(self) -> tuple[tuple[float, float], float]:
        """Get the origin and radius of a circle completely enclosed by the cube's data

        Returns
        -------
        (lat, lon), r
            (lat, lon) - origin of the circle in lat/lon r - radius of the circle
        """
        # Get the location of each sweep in meters relative to the location of the first
        # sweep.
        reflat = self.sweep_lat[0]
        reflon = self.sweep_lon[0]
        X, Y, *_ = pymap3d.geodetic2enu(
            self.sweep_lat, self.sweep_lon, 0, reflat, reflon, 0
        )
        # Get a circle (defined by origin coordinates and a radius) that is enclosed by
        # all the sweeps in the recording, and convert the origin back to a latitude and
        # longitude
        x0, y0, r = largest_circle_in_circles(X, Y, self.info.r_ground.max())
        lat0, lon0, _ = pymap3d.enu2geodetic(x0, y0, 0, reflat, reflon, 0)
        return (lat0, lon0), r


def translate_pol(aziT_src, r_src, xsrc, ysrc, xdst, ydst):
    """Convert azimuth and range vector at source location to azimuth and range matrices
    at destination location."""
    phi_gd_src, r_gd_src = np.meshgrid(azi2phi(aziT_src), r_src, indexing="ij")
    x_gd_src, y_gd_src = pol2cart(phi_gd_src, r_gd_src)
    x_gd = x_gd_src - xsrc
    y_gd = y_gd_src - ysrc
    x_gd_dst = x_gd + xdst
    y_gd_dst = y_gd + ydst
    phi_gd_dst, r_gd_dst = cart2pol(x_gd_dst, y_gd_dst)
    aziT_gd_dst = phi2azi(phi_gd_dst)
    return aziT_gd_dst, r_gd_dst
