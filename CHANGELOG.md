# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.8.7] - 2024-05-16

- Update to `shared-ndarray2` v2.0.1

## [0.8.6] - 2024-04-25

### Fixes

- Call now_utc()
- fix file mode to append when editing wimr `processing_info`
- JSON at least needs empty braces
- When cleaning up after error, don't error if the cube file doesn't exist

## [0.8.5] - 2024-04-24

- Add `update_wimr_proc_info()` method to `BaseCube` and call it from `main.create_cube()`.
  - Assumes the WIMR file's `processing_info` global attribute is valid JSON and loads
    that JSON (or `"{}"`) into a dict.
  - Adds a new key named `"wimr-conversions_<cube_classname>_params_YYYYmmddTHHMMSSZ"`
    with the cube params in JSON-encodable form
  - JSON-dumps the dict back into `processing_info`.

## [0.8.4] - 2024-04-22

Add update_wimr_proc_info to BaseCube

## [0.8.3] - 2024-04-22

Use latest pydantic

## [0.8.2] - 2024-04-19

Minor release, mostly addressing Pydantic 2 deprecations

## [0.8.1] - 2024-04-09

- Don't need pydantic-plus, just use pydantic. nested-config can be a dev dependency

## [0.8.0] - 2023-01-16

- Minimum version Python 3.9
- Fix a number of typing issues
- Create explicit `__init__()` for `ChunkOfSweeps`
- `img_norm()` - add `nanval` param to convert NaNs to a real value and get rid of a runtime warning about
  casting NaN to uint8.
- Minor refactors for clarity
- update `black` -- exponents now don't have spaces
- Update `shared-ndarray2`
- Allow cube start/stop azi to work with recordings that cross 0 azimuth.

## [0.7.11] - 2022-02-09

- Minor refactors, typing fixes, and bump to latest shared-ndarray2

## [0.7.10] - 2022-02-07

- Add overloads to create_cube
- fix shared-ndarray2 compatibility

## [0.7.9] - 2022-02-07

- Use shared-ndarray2
- Address some typing issues and ensure create_cube always returns a Cube

## [0.7.8] - 2021-08-04

- Revert to pydantic-plus

## [0.7.7] - 2021-08-04

- Fix order of classmethod and validate_arguments decorators in BaseModel

## [0.7.6] - 2021-08-03

- Replace pydantic-plus with local module

## [0.7.5] - 2021-07-29

- Refactored add_donut into subfunctions
- Updated to pydantic-plus 1.1.2 - uses rtoml
- Removed pydantic as direct dependency -- get via pydantic-plus instead

## [0.7.4] - 2021-07-06

- Breaking regression bug fix - pydantic root_valdiator must return values

## [0.7.3] - 2021-07-06

- Add value checking to polar cubing params
- Ensure cube.Rg and range_pol are saved as float64.

## [0.7.2] - 2021-06-16

- Fix removal of non-increasing azimuths

## [0.7.1] - 2021-06-10

- Added example script
- Fixed missing cube_path arg/attr

## [0.7.0] - 2021-06-10

- Revamp to object-oriented architecture
- Add moving polar cube type, with MovingPolarParams and gps record ingest
- Made common operations into functions or used numpy built-in (deg2rad, rslant2ground, azi2phi, inverse of those, etc.)
- Moved calculations from params and info structures to the new *Cube classes

## [0.6.3] - 2021-05-21

- Don't allow azi range over 360 deg for PolarCubeParams.

## [0.6.2] - 2021-05-12

- Redifine polar cube.Azi to azi_raw (not azi0)

## [0.6.1] - 2021-05-05

- Fix errors with previous rename to CubeParams

## [0.6.0] - 2021-04-30

- Use pydantic-plus
- Rename CubeConfig stuff to CubeParams

## [0.5.0] - 2021-04-08

- `create_cube` returns a CubeInfo object with the cube path included instead of just the path

## [0.4.2] - 2021-03-23

- Dependency major version updates (h5py 3, utm 0.7)

## [0.4.1] - 2021-03-22

- Replaced `dataclass-params` with `pydantic`

## [0.4.0] - 2021-03-02

- ncvarslice 1.0
- ncdataset 1.0
- Save new `daq_video_inverted` var and `software_versions` attr from WIMR format 1.6 and 1.7
- Use `dataclass-params` to load configs from TOML files into dataclasses

## [0.3.1] - 2021-01-07

- Cube config can be initialized from a config file with `from_toml()` classmethod.

## [0.3.0] - 2021-01-05

- Moved plotting to radar_postproc package
- Minor rework and simplification refactoring.

## [0.2.0] - 2020-12-10

- Raise exception if the cube fails to build.

## [0.1.0] - 2020-12-09

First release.

[Unreleased]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.7...master
[0.8.7]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.6...v0.8.7
[0.8.6]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.5...v0.8.6
[0.8.5]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.4...v0.8.5
[0.8.4]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.3...v0.8.4
[0.8.3]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.2...v0.8.3
[0.8.2]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.1...v0.8.2
[0.8.1]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.11...v0.8.0
[0.7.11]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.10...v0.7.11
[0.7.10]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.9...v0.7.10
[0.7.9]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.8...v0.7.9
[0.7.8]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.7...v0.7.8
[0.7.7]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.6...v0.7.7
[0.7.6]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.5...v0.7.6
[0.7.5]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.4...v0.7.5
[0.7.4]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.3...v0.7.4
[0.7.3]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.2...v0.7.3
[0.7.2]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.1...v0.7.2
[0.7.1]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.6.3...v0.7.0
[0.6.3]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.6.2...v0.6.3
[0.6.2]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.4.2...v0.5.0
[0.4.2]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.3.1...v0.4.0
[0.3.1]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/osu-nrsg/wimr-conversions-python/-/tags/v0.1.0
