import logging
from pathlib import Path
import sys

if sys.version_info < (3, 11):
    import tomli as tomllib  # not installed with wimr-conversions by default
else:
    import tomllib

import wimr_conversions
from wimr_conversions import CartCubeParams

logging.basicConfig(level="DEBUG")


# with open("example_movingpol_params.toml", "rb") as fobj:
#     params = MovingPolarCubeParams.model_validate(tomllib.load(fobj))
# wimr_conversions.create_cube(
#     "/data/recordings/2021-06-12/PointSur_20210612_151803.nc", params
# )


with open("/shared/hpx-radar-runner2/params/wimr-convert/Newport_cart_cubing.toml", "rb") as fobj:
    params = CartCubeParams.model_validate(tomllib.load(fobj))
params.cube_dir = Path("")
wimr_conversions.create_cube(
    "/data/recordings/2024-11-08/NewportTower_20241108_230004.nc", params
)
